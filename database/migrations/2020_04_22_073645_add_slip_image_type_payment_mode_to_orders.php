<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddSlipImageTypePaymentModeToOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('orders', function (Blueprint $table) {
            $table->longText('payment_slip_image')->nullable()->after('deleted_at');
            $table->enum('order_type', ['online', 'onsite'])->nullable()->after('payment_slip_image');
            $table->enum('payment_mode', ['Bank', 'EasyPaisa','Cash'])->nullable()->after('order_type');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('orders', function (Blueprint $table) {
            //
        });
    }
}
