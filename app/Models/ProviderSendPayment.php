<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProviderSendPayment extends Model
{
    public function staff()
    {
        return $this->belongsTo(User::class, 'staff_id', 'id');
    }
}
