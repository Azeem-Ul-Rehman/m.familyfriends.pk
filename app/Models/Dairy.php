<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Dairy extends Model
{
    protected $table = "dairies";
    protected $guarded = [];
}
