<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use SoftDeletes;
    protected $table = "categories";
    protected $guarded = [];

    public function serviceCategories()
    {
        return $this->belongsToMany(ServiceCategory::class, 'pivot_service_categories','category_id','service_id')->withPivot('id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'category_id', 'id');
    }

}
