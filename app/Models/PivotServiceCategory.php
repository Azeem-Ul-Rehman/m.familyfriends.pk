<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class PivotServiceCategory extends Model
{

    public function ServiceCategory()
    {
        return $this->belongsTo(ServiceCategory::class,'service_id','id');
    }

    public function Category()
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }
}
