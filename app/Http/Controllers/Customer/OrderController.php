<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index(Request $request)
    {

        $completed_orders = Order::where('customer_id', auth()->user()->id)->where('order_status', 'completed')->get();
        $pending_orders = Order::where('customer_id', auth()->user()->id)->where('order_status', 'pending')->orWhere('order_status', 'confirmed')->get();

        return view('frontend.customer.appointments', compact('completed_orders', 'pending_orders'));
    }

    public function showOrder($id)
    {
        $order = Order::findOrFail($id);
        return view('frontend.customer.view', compact('order'));

    }
}
