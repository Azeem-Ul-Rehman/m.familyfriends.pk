<?php

namespace App\Http\Controllers\ServiceProvider;


use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
class NotificationController extends Controller
{

    public function index()
    {
        $notifications = Notification::where('user_id',Auth::user()->id)->get();
        return view('frontend.service-provider.notification.index', compact('notifications'));
    }

    
}
