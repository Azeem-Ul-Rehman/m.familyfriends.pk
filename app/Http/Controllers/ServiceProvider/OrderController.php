<?php

namespace App\Http\Controllers\ServiceProvider;

use App\Http\Controllers\Controller;
use App\Http\Resources\Staff\OrderResource as StaffOrderResource;
use App\Models\Order;
use App\Models\OrderReceivedAmount;
use App\Models\User;
use App\Models\Notification;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Helpers\SendSms;
use Session;
use Auth;
class OrderController extends Controller
{
    public function index()
    {
        $check_order_tab = '';
        if(Session::has('ordercheck')){
            $check_order_tab = Session::get('ordercheck');
            Session::forget('ordercheck');
        }
        else{
            $check_order_tab = 'pending';
        }
        $completed_orders = Order::where('order_status','completed')->where('staff_status','completed')->where('staff_id',Auth::user()->id)->get();
        $accepeted_orders = Order::where('staff_status','accepted')->where('staff_id',Auth::user()->id)->get();
        $pending_orders = Order::where('staff_status','pending')->where('staff_id',Auth::user()->id)->get();

        return view('frontend.service-provider.orders.index', compact('completed_orders','check_order_tab','accepeted_orders','pending_orders'));
    }

    public function showOrder($id)
    {
        $order = Order::findOrFail($id);
        $total_paid_amount = OrderReceivedAmount::where('order_id',$id)->sum('received_amount');
        if($total_paid_amount == null){
            $total_paid_amount = 0; 
        }
        return view('frontend.service-provider.orders.show', compact('order','total_paid_amount'));

    }

    public function updateOrderStaffStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'order_staff_status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {
            $user = Auth::user();

            if ($request->order_staff_status == 'accepted') {

                $order = Order::find($request->order_id);
                $order->staff_status = $request->order_staff_status;
                $order->save();
                return response()->json(['status' => "success", 'message' => 'Order status updated successfully.'], 200);
            } else {
                $order = Order::find($request->order_id);
                $order->staff_status = $request->order_staff_status;
                $order->staff_id = null;
                $order->save();
                return response()->json(['status' => "success", 'message' => 'Order status updated successfully.'], 200);
            }


        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function updateOrderProgressStatus(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'order_id' => 'required|exists:orders,id',
            'order_progress_status' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 400);
        }

        try {

            if ($request->order_progress_status == 'on-my-way') {

                $order = Order::find($request->order_id);

                $order->order_progress_status = $request->order_progress_status;
                $order->save();
                $get_customer = User::where('id',$order->customer_id)->first();
                $remove_zero = ltrim($get_customer->phone_number, '0');
                $add_number = '92';
                $phone_number = $add_number . $remove_zero;
                $message = 'Service  Provider on the way against your order# ' . $order->order_id .'. Thank you for using Friends & Family!';
//                event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message)); 
                    $get_customer_id =$get_customer->id;
                    $notification = Notification::Create([
                    'user_id'      =>$get_customer_id ,
                    'message_type'  => 'order',
                    'message'      => $message,
                    ]);
            }


            if ($request->order_progress_status == 'start') {

                $order = Order::find($request->order_id);

                $order->order_progress_status = $request->order_progress_status;
                $order->order_start_time = date('Y-m-d H:i:s');
                $order->save();


            }

            if ($request->order_progress_status == 'end') {

                $order = Order::find($request->order_id);
                $order->order_progress_status = $request->order_progress_status;
                $order->order_end_time = date('Y-m-d H:i:s');


                $minute_price = $order->order_details[0]->amount / 60;
                if ($order->order_details[0]->type == 'Hourly') {
                    $start_time = new DateTime($order->order_start_time);
                    $diff = $start_time->diff(new DateTime($order->order_end_time));
                    $minutes = ($diff->days * 24 * 60) + ($diff->h * 60) + $diff->i;
                    if($order->order_duration <= $minutes){

                    }
                    else{
                        if ($minutes <= 60) {
                            $order->order_duration = 60;
                            $order->total_price = (int)($minute_price * 60);
                        } else {
                            $order->order_duration = $minutes;
                            $order->total_price = (int)($minute_price * $minutes);
                        }
                    }
                } else {
                    $order->total_price = $order->order_details[0]->amount;
                }

                $order->staff_commission = (int)($order->total_price * $order->staff->comission->staff_comission) / 100;
                $order->company_commission = (int)($order->total_price * $order->staff->comission->company_comission) / 100;

                $order->save();

                $get_admin = User::where('user_type','admin')->first();
                $remove_zero = ltrim($get_admin->phone_number, '0');
                $add_number = '92';
                $phone_number = $add_number . $remove_zero;
                $message = 'Service  Provider Completed order# ' . $order->order_id .'. Thank you for using Friends & Family!';
//                event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
               $get_admin_id =$get_admin->id;
               $notification = Notification::Create([
               'user_id'      =>$get_admin_id ,
               'message_type'  => 'order',
               'message'      => $message,
               ]);

            }

            if ($request->order_progress_status == 'collect-cash') {


                $order = Order::find($request->order_id);
                $order->order_progress_status = $request->order_progress_status;
                $order->extra_services = $request->extra_services;
                $order->order_status = 'completed';
                $order->staff_status = 'completed';
                $order->save();
                $get_received_amount = OrderReceivedAmount::where('order_id',$order->id)->sum('received_amount');
                if($get_received_amount == null){
                    $get_received_amount =0;
                }
                if($get_received_amount == $order->total_price){

                }
                else{
                    $order_received_amount = new OrderReceivedAmount();
                    $order_received_amount->order_id = $order->id;
                    $order_received_amount->provider_received_amount = (int)($order->total_price - $get_received_amount);
                    $order_received_amount->description              = 'cash';
                    $order_received_amount->save();
                }
            }


            return response()->json(['status' => 'success', 'message' => 'Order status updated successfully.'], 200);

        } catch (QueryException $exception) {
            return response()->json($exception, 404);
        }
    }

    public function store_order_list_type(Request $request){
        Session::put('ordercheck',$request->type);
        return response()->json(['status' => 'success']);
    }
}
