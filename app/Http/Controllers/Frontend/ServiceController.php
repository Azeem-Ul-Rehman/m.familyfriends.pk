<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\ServiceCategory;
use App\Models\Deal;
use App\Models\Category;
use Facade\Ignition\Support\Packagist\Package;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class ServiceController extends Controller
{
    public function index(Request $request)
    {


        $categories = Category::with('serviceCategories')->get();

//        if (Auth::check()) {
//            if (auth()->user()->hasRole('customer')) {
//
//
//                session()->forget('coupon');
//                $services = ServiceCategory::where('category_id', auth()->user()->category_id)->get();
//                $servicename = Session::get('servicename');
//                $getservicedetail = [];
//                if ($servicename == null) {
//
////                    $getservicedetail = ServiceCategory::first();
////                    $servicename = $getservicedetail->name;
//
//                    $servicename = 'packages';
//
//                } else {
//                    if ($servicename == "packages") {
//                        $getservicedetail == null;
//                    } else {
//                        $getservicedetail = ServiceCategory::where('name', $servicename)->first();
//                    }
//                }
//                return view('frontend.pages.orders.order', compact('services', 'packages', 'servicename', 'getservicedetail'));
//            } else {
//                return view('frontend.pages.service', compact('categories'));
//            }
//        } else {
        return view('frontend.pages.service', compact('categories'));
//        }

    }

    public function serviceItems($category, $slug)
    {
//        dd([$category, $slug]);
        $service_detail = ServiceCategory::where('slug', $slug)->firstOrFail();
        $category = Category::where('name', $category)->first();
        return view('frontend.pages.service-detail-single', compact('service_detail', 'category'));
    }

    public function getservicesname(Request $request)
    {
        $status = "error";

        if ($request->name) {
            Session::put('servicename', $request->name);
            $status = "success";
        }
        return response()->json(['status' => $status]);
    }
}
