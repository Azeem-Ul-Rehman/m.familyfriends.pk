<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Notification;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    public function index()
    {

        $notifications = Notification::where('user_id', auth()->user()->id)->get();
        return view('frontend.customer.notification', compact($notifications));
    }
}
