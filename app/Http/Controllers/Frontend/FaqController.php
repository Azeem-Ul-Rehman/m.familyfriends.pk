<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Aboutus;
use App\Models\Faq;
use App\Models\ManagingPartener;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    public function index()
    {
        $faqs = Faq::take(5)->get();
        return view('frontend.pages.faqs', compact('faqs'));
    }
}
