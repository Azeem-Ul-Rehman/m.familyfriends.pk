<?php

namespace App\Http\Controllers\Auth;

use App\Events\SendReferralCodeWithPhone;
use App\Http\Controllers\Frontend\ApiMessageController;
use App\Http\Controllers\Controller;
use App\Models\City;
use App\Models\Role;
use App\Models\Category;
use App\Models\ServiceCategory;
use App\Models\Staff;
use App\Models\StaffServices;
use App\Models\UserReferral;
use App\Models\UserRole;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\Notification;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cookie;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Helpers\SendSms;
use Illuminate\Validation\Rule;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showRegistrationForm($referral_code = null)
    {
        if ($referral_code) {
            $this->setCookieValue($referral_code);
        }
        $cities = City::all();
        $categories = Category::all();
        return view('auth.register', compact('cities', 'categories', 'referral_code'));
    }

    public function joinAsFamilyFriend($referral_code = null)
    {

        if ($referral_code) {
            $this->setCookieValue($referral_code);
        }
        $cities = City::all();
        $categories = Category::all();
        $services = ServiceCategory::all();
        return view('auth.register-as-family-friend', compact('cities', 'categories', 'referral_code', 'services'));
    }


    public function otp(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'phone_number' => 'required|unique:users,phone_number',
        ]);

        if (!$validator->fails()) {
            try {
                $otp_code = null;

                $otp_code = substr(str_shuffle(str_repeat($x = '0123456789', ceil(10 / strlen($x)))), 1, 4);
                User::create([
                    'otp_code' => $otp_code,
                    'phone_number' => $request->phone_number,
                    'status' => 'suspended'
                ]);
                $remove_zero = ltrim($request->get('phone_number'), '0');
                $add_number = '92';
                $phone_number = $add_number . $remove_zero;

                $message = 'Your OTP Code is ' . $otp_code . ' Please enter this code to verify your mobile number. This message is from Friends & Family';
//            event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));

                $data = [
                    'otp' => $otp_code,
                    'phone_number' => $request->phone_number,
                ];

                return response()->json(['status' => 'success', 'message' => 'OTP code has been sent on given number.', 'data' => $data], 201);

            } catch (QueryException $exception) {
                return response()->json($exception, 400);
            }

        } else {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }


    }

    public function verifyOTP(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'mobile_number' => 'required|exists:users,phone_number',
            'otp_code' => 'required|exists:users,otp_code',
        ]);

        if ($validator->fails()) {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }

        try {


            $user = User::where([
                'phone_number' => $request->get('mobile_number'),
                'otp_code' => $request->get('otp_code'),
            ])->first();

            if (!is_null($user)) {
                $user->otp_status = 'verified';
                $user->status = 'verified';
                $user->save();

                return response()->json(['status' => 'success', 'message' => ''], 200);
            } else {
                return response()->json(['status' => 'error', 'message' => 'Provided OTP code is not correct.'], 401);
            }

        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    public function register(Request $request)
    {
        $random_string = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(10 / strlen($x)))), 1, 10);
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string',
            'last_name' => 'required|string',
            'city_id' => 'required|integer',
            'area_id' => 'required|integer',
            'age' => 'required',
//            'phone_number' => 'required|unique:users,phone_number',
            'cnic' => 'required|unique:users,cnic',
            'category_id' => 'required|integer',
            'address' => 'required',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'password_confirmation' => 'required|same:password',
            'service_category_id.0' => Rule::requiredIf(function () use ($request) {
                if ($request->get('role_id') == 2) {
                    return true;
                }
            }),
        ], [
            'first_name.required' => 'First name field  is required.',
            'last_name.required' => 'Last name field is required.',
            'city_id.required' => 'City field is required.',
            'area_id.required' => 'Area field  is required.',
            'category_id.required' => 'Gender  is required.',
            'age.required' => 'Age field  is required.',
            'phone_number.required' => 'Phone Number field  is required.',
            'cnic.required' => 'CNIC field  is required.',
            'address.required' => 'Address field is required.',
            'email.required' => 'Email field  is required.',
            'service_category_id.0.required' => 'Service  is required.',
        ]);

        if ($validator->fails()) {
            $data = $validator->errors()->toArray();
            $response = (new ApiMessageController())->validatemessage($data, 'Form Has Some Errors.');
            return $response;
        }

        try {
            if ($request->has('image')) {
                $image = $request->file('image');
                $name = $image->getClientOriginalName();
                $destinationPath = public_path('/uploads/user_profiles');
                $imagePath = $destinationPath . "/" . $name;
                $image->move($destinationPath, $name);
                $profile_image = $name;
            } else {
                $profile_image = 'default.png';
            }
            $role = Role::find((int)$request->role_id);


            $user = User::where('phone_number', $request->phone_number)->first();

            $user->update([
                'first_name' => $request->get('first_name'),
                'last_name' => $request->get('last_name'),
                'city_id' => $request->get('city_id'),
                'area_id' => $request->get('area_id'),
                'role_id' => $role->id,
                'age' => $request->get('age'),
                'emergency_number' => $request->get('emergency_number'),
                'cnic' => $request->get('cnic'),
                'address' => $request->get('address'),
                'referral_code' => $random_string,
                'user_type' => $role->name,
                'status' => ($role->id == 2) ? 'suspended' : 'verified',
                'email' => $request->get('email'),
                'password' => Hash::make($request->get('password')),
                'profile_pic' => $profile_image,
            ]);

            UserRole::create([
                'user_id' => $user->id,
                'role_id' => $role->id
            ]);

            //Check the cookie value
            $remove_zero = ltrim($user->phone_number, '0');
            $add_number = '92';
            $phone_number = $add_number . $remove_zero;


            if ($role->id == 2) {

                $staff = Staff::create([
                    'user_id' => $user->id,
                    'full_name' => $user->fullName(),
                    'email' => $request->get('email'),
                    'city_id' => $request->get('city_id'),
                    'area_id' => $request->get('area_id'),
                    'phone_number' => $request->get('phone_number'),
                    'emergency_phone_number' => $request->get('emergency_number'),

                ]);
                if (!empty($request->service_category_id)) {
                    foreach ($request->service_category_id as $key => $service) {
                        StaffServices::create([
                            'staff_id' => $staff->id,
                            'service_category_id' => $service,

                        ]);
                    }
                }
            }


            $message = 'Thank you for becoming a member of Friends & Family! Someone from our team will be calling you shortly to complete your registration process. Thank you!';
            // event(new SendReferralCodeWithPhone($user_name = '', $phone_number, $message));
            $notification = new Notification();
            $notification->user_id      = $user->id;
            $notification->message_type = 'registration';
            $notification->message      = $message;
            $notification->save();

            return response()->json(['status' => 'success', "message" => 'Dear Customer,Thank you for signing up at Friends & Family.'], 200);
        } catch (QueryException $exception) {
            return response()->json($exception, 400);
        }

    }

    private function setCookieValue($referral_code)
    {
        Cookie::queue('referral_code', $referral_code, 1440);
    }

    private function returnCookieValue()
    {
        return Cookie::get('referral_code');
    }

    private function destroyCookie()
    {
        Cookie::queue(Cookie::forget('referral_code'));
    }

    private function getUserWithReferralCode($referral_code)
    {
        return User::where('referral_code', $referral_code)->first()->id;
    }
}
