<?php

namespace App\Http\Controllers;

use App\Models\Dairy;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DairyController extends Controller
{
    public function index($id)
    {
        $dairies = Dairy::where('user_id', $id)->get();
        return view('frontend.service-provider.orders.dairy.index', compact('dairies', 'id'));
    }

    public function create($id)
    {
        return view('frontend.service-provider.orders.dairy.create', compact('id'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'title' => 'required',
            'description' => 'required',
            'user_id' => 'required',
        ], [
            'title.required' => 'Title is required.',
            'description.required' => 'Title is required.'
        ]);
        $dairy = new Dairy();
        $dairy->title = $request->title;
        $dairy->description = $request->description;
        $dairy->user_id = $request->user_id;
        $dairy->name = Auth::user()->fullName();
        $dairy->save();
//        Dairy::create($request->all());
        return redirect()->route('user.dairy.index', $request->user_id)
            ->with([
                'flash_status' => 'success',
                'flash_message' => 'User Dairy created successfully.'
            ]);
    }

    public function show($id)
    {
        $dairy = Dairy::find($id);
        return view('frontend.service-provider.orders.dairy.show', compact('dairy'));
    }
}
