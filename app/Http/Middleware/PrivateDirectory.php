<?php

namespace App\Http\Middleware;

use Closure;
use Session;
class PrivateDirectory
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Session::get('authcheck') == true){
        return $next($request);
        }
        else{
            return redirect('/');
        }
    }
}
