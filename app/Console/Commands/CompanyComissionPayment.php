<?php

namespace App\Console\Commands;

use App\Models\Order;
use App\Models\User;
use Illuminate\Console\Command;

class CompanyComissionPayment extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'company:Comission';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Check Company Comission if Service Provider Pay previous day payment or not ';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
//        $date = \Carbon\Carbon::today()->subDays(1);
        $date = \Carbon\Carbon::today();

        $this->info('Fetching Records.....');
        Order::whereDate('order_end_time', '<', $date)->where('payment_status', 'unpaid')->chunk(100, function ($orders) {
            $orders->each(function ($order) {
                $user = User::where('id', $order->staff_id)->update([
                    'profile_status' => 'defaulter'
                ]);
            });
        });

        $this->info('Command Run Successfully.....');

    }
}
