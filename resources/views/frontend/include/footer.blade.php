<div class="navbar-fixed-bottom">
    <div class="mobMenu">
        <ul class="list-unstyled list-inline">
            @if(Auth::check()==true && Auth::user()->user_type == "staff")
                <li><a
                        class="{{ (request()->is('staff/dashboard')) ? 'activeMenuItem ' : '' }}"
                        href="{{ route('staff.dashboard.index') }}"><img src="{{ asset('frontend/images/icon1.png') }}"
                                                                         alt=""> Home</a>
                </li>
                <li><a class="{{ (request()->is('staff/appointment-history')) ? 'activeMenuItem ' : '' }}"
                       href="{{ route('staff.appointment.history') }}"><img
                            src="{{ asset('frontend/images/icon2.png') }}" alt="">
                        Appointments</a></li>
                <li><a class="{{ (request()->is('staff/notification')) ? 'activeMenuItem ' : '' }}"
                       href="{{ route('customer.notification.index') }}"><img
                            src="{{ asset('frontend/images/icon3.png') }}" alt="">
                        Notifications</a></li>
            @elseif(Auth::check()==true && Auth::user()->user_type == "customer")
                <li><a class="{{ (request()->is('services')) ? 'activeMenuItem ' : '' }}"
                       href="{{ route('service') }}"><img src="{{ asset('frontend/images/icon1.png') }}" alt="">
                        Home</a>
                </li>

                <li><a class="{{ (request()->is('customer/order-history')) ? 'activeMenuItem ' : '' }}"
                       href="{{ route('customer.order.history') }}"><img src="{{ asset('frontend/images/icon2.png') }}"
                                                                         alt="">
                        Appointments</a></li>
                <li><a class="{{ (request()->is('customer/notification')) ? 'activeMenuItem ' : '' }}"
                       href="{{ route('customer.notification.index') }}"><img
                            src="{{ asset('frontend/images/icon3.png') }}" alt="">
                        Notifications</a></li>
            @else
                <li><a class="{{ (request()->is('services')) ? 'activeMenuItem ' : '' }}"
                       href="{{ route('service') }}"><img src="{{ asset('frontend/images/icon1.png') }}" alt="">
                        Home</a>
                </li>
                <li><a class="{{ (request()->is('login')) ? 'activeMenuItem ' : '' }}" href="{{ route('login') }}"><img
                            src="{{ asset('frontend/images/icon2.png') }}" alt="">
                        Appointments</a></li>
                <li><a class="{{ (request()->is('login')) ? 'activeMenuItem ' : '' }}" href="{{ route('login') }}"><img
                            src="{{ asset('frontend/images/icon3.png') }}" alt="">
                        Notifications</a></li>
            @endif
            <li ><a class="{{ (request()->is('menu')) ? 'activeMenuItem ' : '' }}" href="{{ route('menu') }}"><img
                        src="{{ asset('frontend/images/icon4.png') }}" alt=""> Menu</a></li>
        </ul>
    </div>
</div>
