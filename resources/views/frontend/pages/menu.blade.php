@extends('frontend.layout.app')
@push('css')
@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Menu</h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('banner')

@endsection
@section('content')
    <div class="serviceInner">
        <div class="mobMenuScreen">
            <ul class="list-unstyled">
                <li><a href="{{ route('aboutus.detail') }}">About Us</a></li>
                <li><a href="{{ route('faqs') }}">FAQ</a></li>
                <li><a href="{{ route('contacts.create') }}">Contact Us</a></li>
                @if(Auth::check()==true && Auth::user()->user_type == "customer")
                    <li><a href="{{ route('customer.profile.index') }}">Profile</a></li>
                    <li><a href="{{ route('customer.update.password') }}">Change Password</a></li>
                @elseif(Auth::check()==true && Auth::user()->user_type == "staff")
                    <li><a href="{{ route('staff.profile.index') }}">Profile</a></li>
                    <li><a href="{{ route('staff.update.password') }}">Change Password</a></li>
                    <li><a href="{{ route('staff.ledger.index') }}">Ledger</a></li>
                    <li><a href="{{ route('staff.wallet.index') }}">Wallet</a></li>
                @endif
                @if(Auth::check()==true)
                    <li><a href="{{ route('logout') }}"
                           onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>
                    </li>
                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                        {{ csrf_field() }}
                    </form>
                    @else
                    @endif

            </ul>
        </div>
        @if(Auth::check()!=true)
        <div class="mobScreenOpt text-center">
            <h3>Login / SignUp</h3>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="{{ route('login') }}" class="btn btn-primary btnMain btnDetails">As Customer</a>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
                <a href="{{ route('login') }}" class="btn btn-primary btnMain btnDetails">As Staff</a>
            </div>
        </div>
        @else
        @endif
    </div>



@endsection

@push('js')
@endpush

