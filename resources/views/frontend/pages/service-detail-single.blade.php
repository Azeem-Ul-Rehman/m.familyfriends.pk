@extends('frontend.layout.app')
@section('title',$service_detail->meta_title ?? 'Services')
@section('description',$service_detail->meta_description ?? 'Services')
@section('keywords',$service_detail->keywords ?? 'Services')

@push('css')
@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Services</h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('banner')

    <div class="mobBanner">
        <h2>Our Services</h2>
        <p>{{\Illuminate\Support\Str::limit($service_detail->summary,100)}} </p>
    </div>
@endsection
@section('content')
    <div class="serviceInner serviceCatInner">
        <h4>{{ ucfirst($service_detail->name) }}</h4>
        <p>{{ $service_detail->description }} </p>
        <div class="row mrgnRow">
            @if(count($service_detail->sub_categories) > 0)
                @foreach($service_detail->sub_categories as $key=>$sub_category)
                    <div class="col-md-4 col-sm-4 col-xs-4">
                        <div class="serviceBox serviceBox2">
                            <div class="serviceInnerContent">
                                <div class="serviceHeading">
                                    <h3>{{ \Illuminate\Support\Str::limit(ucfirst($sub_category->name),15) }}</h3>
                                    <div class="serviceInnerDet">
                                        <p>{{\Illuminate\Support\Str::limit($sub_category->description,50)}}</p>
                                        <h4>Rs. {{ number_format($sub_category->price)}}
                                            <span>{{($sub_category->service_type == 'hourly') ? 'Per Service' :''}}</span>
                                        </h4>
                                        @if(auth()->check() && auth()->user()->user_type  == 'customer')
                                            <a href="{{route('appointment',['category'=>$category->name,'slug'=>$service_detail->slug,'subcategory'=>$sub_category->id])}}"
                                               class="btn btn-primary btnMain">Schedule Now</a>
                                        @elseif(auth()->check() && auth()->user()->user_type  == 'admin')
                                        @elseif(auth()->check() && auth()->user()->user_type  == 'staff')
                                        @else
                                            <a href="{{route('login')}}"
                                               class="btn btn-primary btnMain">Schedule Now</a>
                                        @endif

                                        {{--                                        <a href="{{route('appointment',['category'=>$category->name,'slug'=>$service_detail->slug,'subcategory'=>$sub_category->id])}}"--}}
                                        {{--                                           class="btn btn-primary btnMain">Schedule Now</a>--}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif

        </div>
    </div>
@endsection
@push('js')
    <script>

        function saveOrder(categoryId, serviceId, subCategoryId) {

            var request = {
                "service_id": serviceId,
                "sub_category_id": subCategoryId,
            };

            {{--$.ajax({--}}
            {{--    type: "GET",--}}
            {{--    url: "{{ route('appointment') }}",--}}
            {{--    data: request,--}}
            {{--    dataType: "json",--}}
            {{--    cache: true,--}}
            {{--    success: function (response) {--}}

            {{--        if (response.flash_status == "success") {--}}
            {{--            window.location.href = '{{ route('appointment') }}';--}}
            {{--        } else {--}}
            {{--            toastr[response.flash_status](response.flash_message);--}}
            {{--        }--}}
            {{--    },--}}
            {{--    error: function () {--}}
            {{--        toastr['error']("Something Went Wrong.");--}}
            {{--    }--}}
            {{--});--}}

        }
    </script>

@endpush
