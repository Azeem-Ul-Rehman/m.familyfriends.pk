@extends('frontend.layout.app')

@push('css')

@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Contact us</h3>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection
@section('content')
    <div class="serviceInner">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 aboutContent">
                <h3 class="sectionHeading">CONTACT INFO</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                <div class="contactLeft">
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <i class="fas fa-phone"></i>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <address>Phone <span>{{ $settings[0]->value }}</span></address>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="col-md-2 col-sm-2 col-xs-2">
                                <i class="fas fa-envelope"></i>
                            </div>
                            <div class="col-md-10 col-sm-10 col-xs-10">
                                <address>Email <span>{{ $settings[1]->value }}</span></address>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="col-md-1 col-sm-2 col-xs-2">
                                <i class="fas fa-map-marker-alt"></i>
                            </div>
                            <div class="col-md-11 col-sm-10 col-xs-10">
                                <address>Address <span>{{ $settings[2]->value }}</span></address>
                            </div>
                        </div>
                    </div>
                    <hr style="border: #bababa solid 1px; width: 100%;">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="list-unstyled list-inline">
                                <li><a href="{{$settings[10]->value}}" target="_blank"><i class="fab fa-facebook"></i></a></li>
                                <li><a href="{{$settings[9]->value}}" target="_blank"><i class="fab fa-instagram"></i></a></li>
                                <li><a href="{{$settings[8]->value}}" target="_blank"><i class="fab fa-twitter"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12 aboutContent">
                <h3 class="sectionHeading">SEND A MESSAGE</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting </p>
                <form method="post" action="{{ route('contacts.store') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="text" name="full_name"
                                       class="form-control @error('full_name') is-invalid @enderror"
                                       placeholder="Name*" value="{{ old('full_name') }}" autocomplete="full_name">
                                @error('full_name')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <input type="email" name="email"
                                       class="form-control @error('email') is-invalid @enderror"
                                       placeholder="Email*" value="{{ old('email') }}" autocomplete="email">
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <input id="subject" type="text"
                                       class="form-control @error('subject') is-invalid @enderror"
                                       name="subject" value="{{ old('subject') }}" autocomplete="subject"
                                       placeholder="Subject*">

                                @error('subject')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                               <textarea id="message"
                                         class="form-control @error('message') is-invalid @enderror"
                                         name="message" rows="5"
                                         autocomplete="message">{{ old('message') }}</textarea>

                                @error('message')
                                <span class="invalid-feedback" role="alert">
                                              <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btnMain btnDetails">Submit</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $(document).ready(function () {
            $('.navbar-fixed-bottom').hide();
        })
    </script>
@endpush

