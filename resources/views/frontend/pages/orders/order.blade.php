@extends('frontend.layout.app')
@section('title','Orders')

@section('header')
    <div class="mobHeader">
        <div class="col-md-2 col-sm-2 col-xs-2">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-8 col-sm-8 col-xs-8">
            <h3>Schedule Your Appointment</h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('banner')

@endsection
@push('css')
    <style>
        .max-width {
            max-width: 100%;
        }
    </style>

@endpush
@section('content')
    <div class="serviceInner serviceCatInner">
        <div class="col-md-12 col-sm-12 col-xs-12 appointmentLeft">
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12 appointLgImg">
                    <img class="max-width" src="{{ $menuItem->image }}" alt="{{$menuItem->image}}">
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h4 id="name">{{ucfirst($menuItem->name)}}</h4>
                    <h5>PKR <span id="price">{{ number_format($menuItem->price) }}</span>/- <span>{{($menuItem->service_type == 'hourly') ? 'Hourly' :'Fixed'}} service</span>
                    </h5>
                    <p>{{\Illuminate\Support\Str::limit($menuItem->description,150)}}</p>
                    @if($menuItem->description != null && strlen($menuItem->description) > 50)
                        <div class="text-right detLink">
                            <a href="javascript:void(0)" class="readMore"
                               data-description="{{$menuItem->description}}"
                               id="readMore">Read
                                More</a>
                        </div>
                    @else
                    @endif
                </div>
                <div class="clearfix"></div>
            </div>

        </div>
        <form action="#" method="get">
            <div class="col-md-12 appointmentLeft mrgnRow">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <label>Appointment type: ({{ $category->name }})</label>

                        <input type="hidden" value="0" name="time_zone" id="time_zone">
                        <input type="hidden" value="{{ lcfirst($category->name ) }}" name="order_type"
                               id="order_type">
                        <input type="hidden" value="{{ $category->id  }}" name="category_id"
                               id="category_id">
                        <input type="hidden" value="{{ $service->id  }}" name="service_id"
                               id="service_id">
                        <input type="hidden" value="{{ $menuItem->id  }}" name="menu_item_id"
                               id="menu_item_id">

                        <input type="hidden" value="{{ $menuItem->name  }}" name="menu_item_name"
                               id="menu_item_name">
                        <input type="hidden" value="{{ ucfirst($menuItem->service_type)  }}" name="service_type"
                               id="service_type">
                        <div class="form-group">

                            <label for="datetimepicker1">Date <span style="color: red">*</span></label>
                            <input type="text" name="datetimepicker1" id="datetimepicker1"
                                   class="form-control @error('datetimepicker1') is-invalid @enderror"
                                   required autocomplete="datetimepicker1" onkeypress="return false;">
                            @error('datetimepicker1')
                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>
                        <div class="form-group">

                            <label for="time">Time <span style="color: red">*</span></label>
                            <input type="text" name="time" id="time"
                                   class="form-control @error('time') is-invalid @enderror"
                                   required autocomplete="time" onkeypress="return false;">
                            @error('time')
                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                            @enderror
                        </div>

                        @if($menuItem->service_type == 'hourly')
                            <div class="form-group">

                                <label for="duration">Duration</label>
                                <select class="form-control" name="duration" id="duration">
                                    <option value="1">1 h</option>
                                    <option value="2">2 h</option>
                                    <option value="3">3 h</option>
                                    <option value="4">4 h</option>
                                    <option value="5">5 h</option>
                                    <option value="6">6 h</option>
                                    <option value="7">7 h</option>
                                    <option value="8">8 h</option>
                                    <option value="9">9 h</option>
                                    <option value="10">10 h</option>
                                </select>
                            </div>
                        @else
                            <input type="hidden" name="duration" value="0" id="duration">
                        @endif
                        <div class="form-group">
                            <label>Payment option</label>
                            <div class="row">
                                @if($category->name == 'Onsite')
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label><input type="radio" name="payment" id="payment" value="Cash"
                                                          checked> &nbsp; Cash on Delivery</label>
                                        </div>
                                    </div>
                                @else
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label><input type="radio" name="payment" id="payment" value="Bank"
                                                          checked> &nbsp; Payment via Bank</label>
                                        </div>
                                    </div>
                                    <div class="col-md-4 col-sm-4 col-xs-12">
                                        <div class="form-group">
                                            <label><input type="radio" name="payment" id="payment"
                                                          value="EasyPaisa"> &nbsp; EasyPaisa</label>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>


                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h4>Bill Details</h4>
                        <div class="appointmentCalculations">
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h5>{{$menuItem->name}}</h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                    <h5><span>PKR <span
                                                id="single-price">{{number_format($menuItem->price)}}</span>/-</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h5>Delivery charges</h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                    <h5><span>PKR <span
                                                id="delivery_charges">{{ ($category->name == 'Online') ? 0 : $user->area->price }}</span>/-</span>
                                    </h5>
                                </div>
                            </div>
                            <div class="row totalRow">
                                <div class="col-md-6 col-sm-6 col-xs-6">
                                    <h5>Total</h5>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                                    <h5><span>PKR <span
                                                id="total-price">{{ number_format(($menuItem->price + (($category->name == 'Online') ? 0 : $user->area->price))) }}</span> /-</span>
                                    </h5>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        @if($category->name == 'Online')
                            <h4>Bank Details</h4>
                            <ol>
                                <li>Lorem Isum is simply dummy text of the printing and typesetting industry. Lorem
                                    Ipsum
                                    has been the
                                </li>
                                <li>try's standard dummy text ever since the 1500s, when an unknown printer took a
                                    galley of
                                    type and scrambled it to make a type specimen book.
                                </li>
                                <li>try's standard dummy text ever since the 1500s, when an unknown printer took a
                                    galley of
                                    type and scrambled it to make a type specimen book. ook a galley of type and
                                    scrambled
                                    it to make a type specimen book.
                                </li>
                            </ol>

                            <div class="form-group files">
                                <input type="file" class="form-control" name="payment_slip" id="payment_slip"
                                       {{ ($category->name == 'Online') ? 'required' : '' }}
                                       onchange="readURL(this);">
                                <div>
                                    <img src="" id="bannerImg" style="display: none">
                                </div>
                            </div>
                        @endif
                        <div class="form-group">
                            <input type="button" class="btn btn-primary btnMain btnDetails" value="Proceed"
                                   onclick="orderReview()"
                            />
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </form>
        <div class="clearfix"></div>
    </div>

@endsection

@push('models')

    <div id="description_modal" class="modal fade mw-100 w-75" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Service Description</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form class="m-form" method="post" role="form">
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">
                                <p id="description"></p>
                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                            <div class="m-form__actions m-form__actions">
                                <a href="#" class="btn btn-info" data-dismiss="modal">Close</a>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
@endpush


@push('js')
    <script>

        $('#duration').on('change', function () {

            var duration = parseInt($('#duration').val());
            var price = parseInt($('#price').text().replace(/,/g, ""));
            var deliveryCharges = parseInt($('#delivery_charges').text());

            var totalPrice = 0;
            var singlePrice = 0;
            totalPrice = (duration * price) + deliveryCharges;
            singlePrice = (duration * price);

            $('#total-price').text(totalPrice);
            $('#single-price').text(singlePrice);

        });
        let services;
        $(document).ready(function () {
            $('#time_zone').val(Intl.DateTimeFormat().resolvedOptions().timeZone);

            function dateTimePicker(dateID, timeID) {
                var dateToday = new Date();
                $(dateID).datetimepicker({
                    format: "MM-DD-YYYY",
                    minDate: moment().add(0, 'hours'),
                    icons: {
                        time: 'fa fa-clock',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-check',
                        clear: 'fa fa-trash',
                        close: 'fa fa-times'
                    }
                });
                var startTime = '09:15am';
                var endTime = '08:15pm';
                var startNewTime = moment(startTime, "HH:mm a");
                var endNewTime = moment(endTime, "HH:mm a");
                $(timeID).datetimepicker({
                    format: "hh:mm A",
                    stepping: 15, //will change increments to 15m, default is 1m
                    icons: {
                        time: 'fa fa-clock',
                        date: 'fa fa-calendar',
                        up: 'fa fa-chevron-up',
                        down: 'fa fa-chevron-down',
                        previous: 'fa fa-chevron-left',
                        next: 'fa fa-chevron-right',
                        today: 'fa fa-check',
                        clear: 'fa fa-trash',
                        close: 'fa fa-times'
                    }
                }).on('dp.change', function (e) {
                    var date = e.date;//e.date is a moment object
                    var target = $(e.target).attr('name');
                    if (moment().format('MM-DD-YYYY') == $(dateID).val()) {
                        $('#time').val(date.add(0, 'hours').format('hh:mm A'));
                    } else {
                        $('#time').val(date.format('hh:mm A'));
                    }
                });
            }

            dateTimePicker('#datetimepicker1', '#time');
        });

        function orderReview() {


            if ($('#datetimepicker1').val() == '') {
                toastr['error']("Date is required");
            } else if ($('#time').val() == '') {
                toastr['error']("Time is required");
            } else if ($('#duration').val() == '' && $('#service_type').val() == 'Hourly') {
                toastr['error']("Duration is required");
            } else if ($('#payment_slip').val() == '' && $('#order_type').val() == 'online') {
                toastr['error']("Image is required");
            } else {
                saveDataInStorage();
            }


        }

        function saveDataInStorage() {
            var paymentMode = null;
            var paymentSlip = null;
            if ($('#payment').is(':checked')) {
                paymentMode = $('#payment').val();
            }


            if ($('#order_type').val() == 'online') {
                bannerImage = document.getElementById('bannerImg');
                imgData = getBase64Image(bannerImage);
                paymentSlip = "data:image/png;base64," + imgData;
            } else {
                paymentSlip = null;
            }
            var newServices = {
                category_id: $('#category_id').val(),
                service_id: $('#service_id').val(),
                menu_item_id: $('#menu_item_id').val(),
                name: $('#menu_item_name').val(),
                price: parseInt($('#price').text().replace(/,/g, "")),
                timeZone: $('#time_zone').val(),
                duration: $('#duration').val(),
                requestedDatetime: $('#datetimepicker1').val(),
                time: $('#time').val(),
                alternateAddress: '',
                order_type: $('#order_type').val(),
                totalPrice: parseInt($('#single-price').text()),
                payment_mode: paymentMode,
                service_type: $('#service_type').val(),
                payment_slip: paymentSlip
            };

            services = JSON.parse(localStorage.getItem('services'));
            if (services == null) {
                services = null;
            }
            services = newServices;
            localStorage.setItem('services', JSON.stringify(services));


            var finalOrder = JSON.parse(localStorage.getItem('services'));

            if (finalOrder != null) {
                window.location.href = '{{ route('appointment.review',[$category->name,$service->slug,$menuItem->id]) }}';
                {{--$.ajax({--}}
                {{--    type: "GET",--}}
                {{--    url: "{{ route('appointment.review',[$category->name,$service->slug,$menuItem->id]) }}",--}}
                {{--    data: {--}}
                {{--        'finalOrder': finalOrder,--}}
                {{--    },--}}
                {{--    dataType: "json",--}}
                {{--    cache: false,--}}
                {{--    success: function (response) {--}}

                {{--        if (response.data.flash_status == "success") {--}}
                {{--            window.location.href = '{{ route('appointment.review',[$category->name,$service->slug,$menuItem->id]) }}';--}}
                {{--        }--}}
                {{--    },--}}
                {{--    error: function () {--}}
                {{--        toastr['error']("Something Went Wrong.");--}}
                {{--    }--}}
                {{--});--}}
            } else {
                toastr['error']("You didn't have selected any item yet.");
            }
        }

        function readURL(input) {
            console.log(input);
            if (input.files && input.files[0]) {

                let size = input.files[0].size;
                console.log(`Image Size: ${size}`);

                var reader = new FileReader();
                reader.onload = function (e) {


                    if (size > 2000000) {
                        $('#bannerImg').attr('src', '');
                        $('#payment_slip').val('');

                        toastr.warning(" Image Size is exceeding 2 Mb");


                    } else {
                        $('#bannerImg').attr('src', e.target.result);
                        $('#bannerImg').css("display", "block");
                        $('#hidden-field').val('');
                    }


                };
                reader.readAsDataURL(input.files[0]);
            }
        }


        function getBase64Image(img) {
            var canvas = document.createElement("canvas");
            canvas.width = img.width;
            canvas.height = img.height;

            var ctx = canvas.getContext("2d");
            ctx.drawImage(img, 0, 0, img.width, img.height);

            var dataURL = canvas.toDataURL("image/png");

            return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
        }

        $(document).on('click', '.readMore', function () {
            var $e = $(this);
            $("#description").html($e.data('description'));
            $("#description_modal").modal('show');
        });


    </script>

@endpush
