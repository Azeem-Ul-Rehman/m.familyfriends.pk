@extends('frontend.layout.app')
@push('css')
@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>About us</h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('banner')

@endsection
@section('content')
    <div class="serviceInner">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 aboutContent">
                <h3>A Great Place to Work. <br> A Great Place to Receive Care.</h3>
                {!! ($aboutus) ? $aboutus->description : '' !!}
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <img src="{{ asset('frontend/images/aboutImg.png') }}" alt="" width="100%">
            </div>
        </div>
    </div>



@endsection

@push('js')
@endpush

