@extends('frontend.layout.app')

@push('css')
@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            {{--<a href="{{ URL::previous() }}">←</a>--}}
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Services</h3>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection
@section('banner')

    <div class="mobBanner">
        <h2>Our Services</h2>
        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
    </div>

@endsection
@section('content')
    <div class="serviceInner">
        <ul class="nav nav-tabs">


            @foreach($categories as $category)
                <li class="{{ ($category->name == 'Onsite') ? 'active' : '' }}">
                    <a data-toggle="tab" href="#{{ ucfirst($category->name) }}">
                        {{  ucfirst($category->name) }}
                    </a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">
            @if(!empty($categories))
                @foreach($categories as $category)
                    <div id="{{ucfirst($category->name)}}"
                         class="tab-pane fade in {{ ($category->name == 'Onsite') ? 'active' : '' }}">
                        <div class="servicesMain">
                            <div class="row">
                                @if(!empty($category->serviceCategories))
                                    @foreach($category->serviceCategories as $service)
                                        <div class="col-md-4 col-sm-4 col-xs-4">
                                            <div class="serviceBox">
                                                <a href="{{ route('service.items',[$category->name, $service->slug]) }}">
                                                    <div class="overlay"></div>
                                                    <img
                                                        src="{{$service->thumbnail_image }}"
                                                        alt="{{$service->thumbnail_image}}">
                                                    <div class="serviceContent">
                                                        <div class="serviceIcon">
                                                            <img
                                                                src="{{$service->icon }}"
                                                                alt="{{$service->icon}}">
                                                        </div>
                                                        <div class="serviceHeading">
                                                            <h3>{{ $service->name }}</h3>
                                                        </div>
                                                    </div>
                                                </a>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                @endforeach
            @endif
        </div>
    </div>
@endsection
@push('js')
    <script>
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': '{{ csrf_token() }}'
            }
        });

        function getServiceName(name) {
            $.ajax({
                url: "{{ route('getservicesname') }}",
                type: 'POST',
                data: {name: name},
                success: function (response) {
                    if (response.status == 'success') {
                        window.location.href = "{{route('service')}}";
                    } else {
                        toastr['error']("Something went wrong.");
                    }
                },
                error: function (error) {
                    toastr['error']("Something went wrong.");
                }

            });
        }
    </script>
@endpush

