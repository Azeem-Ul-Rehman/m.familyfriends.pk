@extends('frontend.layout.app')
@push('css')
@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>FAQ</h3>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection
@section('content')

    <div class="serviceInner">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 aboutContent">
                <h3>HAVE ANY QUESTIONS ?</h3>
                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text. </p>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="panel-group" id="accordion">
                    @foreach($faqs as $key =>$faq)
                    <div class="panel panel-default">
                        <div class="panel-heading {{ ($loop->index == 0) ?'active' : '' }}">
                            <h4 class="panel-title">
                                <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse{{$key}}">
                                    <span>?</span> {{ $faq->title }} 
                                </a>
                            </h4>
                        </div>
                        <div id="collapse{{$key}}"
                             class="panel-collapse collapse {{ ($loop->index == 0) ?'in' : '' }}">
                            <div class="panel-body">
                                {{ $faq->description }}
                            </div>
                        </div>
                    </div>
                @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('.navbar-fixed-bottom').hide();
        })
    </script>
@endpush

