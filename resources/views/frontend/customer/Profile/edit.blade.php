@extends('frontend.layout.app')

@push('css')
    <style>
        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }
    </style>
@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Edit Profile</h3>
        </div>
{{--        <div class="col-md-4 col-sm-4 col-xs-4 text-right">--}}
{{--            <a href="#"><img src="images/profileEdit.png" alt=""></a>--}}
{{--        </div>--}}
        <div class="clearfix"></div>
    </div>
@endsection

@section('content')
    <div class="serviceInner notifications profileInfo">
        <div class="row form-group">
            <div class="col-md-2 col-sm-2 col-xs-3">

                <img class="profile-image"
                     src="{{$user->profile_pic}}"
                     alt="{{$user->profile_pic}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2>{{ $user->fullName() }}</h2>
                <h2><span>{{ $user->email }}</span></h2>
            </div>
        </div>
        <form action="{{ route('customer.update.user.profile') }}"
              method="post"
              enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input type="text" id="first_name" name="first_name"
                               class="form-control"
                               value="{{ $user->first_name }}">
                        @error('first_name')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input type="text" class="form-control"
                               name="last_name" id="last_name"
                               value="{{ $user->last_name }}">
                        @error('last_name')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" class="form-control" id="email"
                               name="email" value="{{ $user->email }}">
                        @error('email')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Gender</label>
                        <select class="form-control" name="gender">
                            <option value="Male" {{ ($user->gender == 'Male') ? 'selected' : '' }}>Male</option>
                            <option value="Female" {{ ($user->gender == 'Female') ? 'selected' : '' }}>Female</option>
                        </select>
                        @error('gender')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Mobile No.</label>
                        <input type="text" name="phone_number" class="form-control disabledDiv" value="{{ $user->phone_number }}">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="cnic">CNIC </label>
                        <input type="text" class="form-control disabledDiv" name="cnic"
                               id="cnic" value="{{ $user->cnic }}">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" class="form-control"
                               name="address" id="address"
                               value="{{ $user->address }}">
                        @error('address')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="city_id">City </label>
                        <select class="form-control cities"
                                name="city_id"
                                id="city_id">
                            @foreach($cities as $city)
                                <option
                                    value="{{ $city->id }}" {{ ($city->id == $user->city_id) ? 'selected' : ''}}>{{ ucfirst($city->name) }}</option>
                            @endforeach
                        </select>
                        @error('city_id')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="area_id">Area </label>
                        <select class="form-control areas"
                                id="area_id"
                                name="area_id">
                            @foreach($areas as $area)
                                <option
                                    value="{{ $area->id }}" {{ ($area->id == $user->area_id) ? 'selected' : ''}}>{{ ucfirst($area->name) }}</option>
                            @endforeach
                        </select>
                        @error('area_id')
                        <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btnMain btnDetails">Save Changes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>

@endsection

@push('js')
    <script>
        $('.cities').change(function () {
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.areas';
            var city_id = $(this).val();
            var request = "city_id=" + city_id;

            if (city_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.cityAreas') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $.each(response.data.area, function (i, obj) {
                                html += '<option value="' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select Area</option>");
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value='' selected>Select Area</option>");
            }
        });
    </script>
@endpush
