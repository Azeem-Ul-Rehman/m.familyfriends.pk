@extends('frontend.layout.app')

@push('css')
@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Change Password</h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('content')
    <div class="serviceInner notifications profileInfo">

        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                @if (session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif
                @if (session('success'))
                    <div class="alert alert-success">
                        {{ session('success') }}
                    </div>
                @endif
            </div>
        </div>
        <form action="{{ route('customer.update.user.password') }}"
              method="post" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div
                        class="form-group {{ $errors->has('current-password') ? ' has-error' : '' }}">
                        <label for="current-password">Current
                            Password</label>
                        <input type="password" class="form-control"
                               name="current-password"
                               id="current-password">
                        @if ($errors->has('current-password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('current-password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div
                        class="form-group {{ $errors->has('new-password') ? ' has-error' : '' }}">
                        <label for="new-password">New Password</label>
                        <input type="password" class="form-control"
                               name="new-password" id="new-password">
                        @if ($errors->has('new-password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('new-password') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label for="new-password-confirm">Re-type
                            Password</label>
                        <input type="password" class="form-control"
                               name="new-password-confirm"
                               id="new-password-confirm">
                    </div>
                </div>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btnMain btnDetails">Save Changes</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
@endsection

@push('js')
@endpush
