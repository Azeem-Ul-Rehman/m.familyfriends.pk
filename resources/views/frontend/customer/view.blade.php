@extends('frontend.layout.app')

@push('css')
    <style>
        .max-width {
            max-width: 100%;
        }
    </style>
@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-3">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-6">
            <h3>Order Details</h3>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection

@section('content')
    <div class="serviceInner serviceCatInner">
        <div class="col-md-12 col-sm-12 col-xs-12 appointmentLeft orderReviewLeft">
            <div class="personalInfo">
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <h5>{{ $order->user->fullName() }} </h5>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="outputRes">Time:
                            <span>{{\Carbon\Carbon::parse($order->order_details[0]->time)->format('h:i A')}}</span></p>
                        <p class="outputRes">Date:
                            <span>{{\Carbon\Carbon::parse($order->order_details[0]->date)->setTimezone($order->time_zone)->format('m/d/Y')}}     </span>
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <h5>Contact No. <span>{{ $order->phone_number }}    </span></h5>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <h5>Email <span>{{ $order->user->email }}</span></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <p class="outputRes">Payment option: </p>
                        <p class="outputRes"><span>{{ ucfirst($order->payment_mode)  }}</span></p>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-6">
                        <label>Appointment type: <br> ({{ ucfirst($order->order_details[0]->category->name)  }})</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 form-group">
                        <h5>Address <span>{{  $order->user->address }}
                                                                            , {{ $order->area->name }}
                                                                            , {{ $order->city->name }}</span>
                        </h5>
                    </div>
                </div>
                @if(!is_null($order->order_details[0]->alternate_address ))

                    <div class="row">
                        <div class="col-md-12 form-group">
                            <h5>Alternate <span>{{ $order->order_details[0]->alternate_address  }}</span>
                            </h5>
                        </div>
                    </div>
                @endif
                @if(!is_null($order->special_instruction ))

                    <div class="row">
                        <div class="col-md-12 form-group">
                            <h5>Special Instructions</h5>
                            <span>{{ $order->special_instruction  }}</span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="row form-group">
                <div class="col-md-4 col-sm-4 col-xs-4">
                    <img class="max-width" src="{{ $order->order_details[0]->sub_category->image }}" alt="">
                </div>
                <div class="col-md-8 col-sm-8 col-xs-8">
                    <h4>{{ $order->order_details[0]->sub_category->name }}</h4>
                    <p>{{ $order->order_details[0]->sub_category->description }} </p>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="statusBar">
                        @if($order->order_status == 'pending')
                            <div class="col-md-4 col-sm-4 col-xs-4 statusBarPending">
                                <p>Pending</p>
                            </div>
                        @elseif($order->order_status == 'confirmed')
                            <div class="col-md-4 col-sm-4 col-xs-4 ">

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 statusBarActive">
                                <p>Active</p>
                            </div>
                        @else
                            <div class="col-md-4 col-sm-4 col-xs-4 ">

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 ">

                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-4 statusBarConfirmed">
                                <p>Confirmed</p>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@push('js')
    <script>
        $(document).ready(function () {
            $('.navbar-fixed-bottom').hide();
        })
    </script>
@endpush
