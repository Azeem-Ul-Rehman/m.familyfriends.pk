@extends('frontend.layout.app')

@push('css')
@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Appointments</h3>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection

@section('content')

    <div class="serviceInner">
        <ul class="nav nav-tabs appointmentsTabs">
            <li class="active"><a data-toggle="tab" href="#new">New</a></li>
            <li><a data-toggle="tab" href="#history">History</a></li>
        </ul>
        <div class="tab-content">
            <div id="new" class="tab-pane fade in active">
                <div class="servicesMain">
                    <div class="row">
                        @if(!empty($pending_orders) && (count($pending_orders) > 0))
                            @foreach($pending_orders as $pending_order)
                                @if(!is_null($pending_order->order_details) && count($pending_order->order_details) >0)

                                    <div class="appointmentRow">
                                        <div class="col-md-3 col-sm-3 col-xs-3 appointLeft">
                                            @if(!is_null($pending_order->order_details[0]->sub_category))
                                                <img src="{{ $pending_order->order_details[0]->sub_category->image }}"
                                                     alt="{{$pending_order->order_details[0]->sub_category->image}}">
                                            @else
                                                <img src="{{ asset('frontend/images/profileImg.png') }}"
                                                     alt="profileImg">
                                            @endif
                                        </div>
                                        <div class="col-md-9 col-sm-9 col-xs-9 appointRight">
                                            <h4>{{ $pending_order->order_details[0]->name }}</h4>
                                            <ul class="list-unstyled list-inline">
                                                <li>{{ ucfirst($pending_order->order_details[0]->service_category->name)}}</li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <p> {{\Carbon\Carbon::parse($pending_order->order_details[0]->time)->format('h:i A')}}</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <p>{{\Carbon\Carbon::parse($pending_order->order_details[0]->date)->setTimezone($pending_order->time_zone)->format('m/d/Y')}} </p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <p>
                                                        Rs. {{ $pending_order->total_price + (int)$pending_order->delivery_charges }}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <h3 class="{{$pending_order->order_status == "confirmed" ? 'statusConfirmed' : 'statusPending'}}">{{ ucfirst($pending_order->order_status) }}</h3>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 viewDet">
                                                    <a href="{{route('user.dairy.index',$pending_order->user->id)}}">Dairy</a>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 viewDet">
                                                    <a href="{{route('customer.user.order.show',$pending_order->id)}}">View
                                                        Details</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div id="history" class="tab-pane fade">
                <div class="servicesMain">
                    <div class="row">
                        @if(!empty($completed_orders) && (count($completed_orders) > 0))
                            @foreach($completed_orders as $completed_order)
                                @if(!is_null($completed_order->order_details) && count($completed_order->order_details) >0)

                                    <div class="appointmentRow">
                                        <div class="col-md-3 col-sm-3 col-xs-3 appointLeft">
                                            @if(!is_null($completed_order->order_details[0]->sub_category))
                                                <img
                                                    src="{{ $completed_order->order_details[0]->sub_category->image }}"
                                                    alt="{{$completed_order->order_details[0]->sub_category->image}}">
                                            @else
                                                <img src="{{ asset('frontend/images/profileImg.png') }}"
                                                     alt="profileImg">
                                            @endif
                                        </div>
                                        <div class="col-md-9 col-sm-9 col-xs-9 appointRight">
                                            <h4>{{ucfirst( $completed_order->order_details[0]->name) }}</h4>
                                            <ul class="list-unstyled list-inline">
                                                <li>{{ $completed_order->order_details[0]->service_category->name}}</li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <p> {{\Carbon\Carbon::parse($completed_order->order_details[0]->time)->format('h:i A')}}</p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <p>{{\Carbon\Carbon::parse($completed_order->order_details[0]->date)->setTimezone($completed_order->time_zone)->format('m/d/Y')}} </p>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <p>
                                                        Rs. {{ $completed_order->total_price + (int)$completed_order->delivery_charges }}</p>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-6 col-sm-6 col-xs-6">
                                                    <h3 class="statusConfirmed">{{ ucfirst($completed_order->order_status) }}</h3>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 viewDet">
                                                    <a href="{{route('user.dairy.index',$completed_order->user->id)}}">Dairy</a>
                                                </div>
                                                <div class="col-md-6 col-sm-6 col-xs-6 viewDet">
                                                    <a href="{{route('customer.user.order.show',$completed_order->id)}}">View
                                                        Details</a>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                @endif
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
@endpush
