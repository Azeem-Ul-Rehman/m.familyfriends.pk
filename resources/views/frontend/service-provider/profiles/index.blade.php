@extends('frontend.layout.app')

@push('css')
@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Profile</h3>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4 text-right">
            <a href="{{ route('staff.profile.edit') }}"><img src="{{ asset('frontend/images/profileEdit.png') }}" alt=""></a>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('content')
    <div class="serviceInner notifications profileInfo">
        <div class="row form-group">
            <div class="col-md-2 col-sm-2 col-xs-3">
                <form method="post" enctype="multipart/form-data" id="submit-image"
                      action="javascript:void(0)">

                    @csrf
                    <input type="hidden" name="user_id" id="user_id"
                           value="{{ $user->id}}">

                    <label for="image">
                        <img src="http://goo.gl/pB9rpQ"
                             style="width: 40px;cursor: pointer;"/>
                    </label>

                    <input type="file" name="image" id="image" style="display: none"
                           onchange="readURL(this);"
                           accept=".png, .jpg, .jpeg"/>

                </form>

                <img class="profile-image"
                     src="{{$user->profile_pic}}"
                     alt="{{$user->profile_pic}}">
            </div>
        </div>
        <div class="row form-group">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2>{{ $user->fullName() }}</h2>
                <h2><span>{{ $user->email ?? '--' }}</span></h2>
            </div>
        </div>
        <div class="personalInfo">
            <div class="row">
                <div class="col-md-12 form-group">
                    <h5>Contact No. <span>{{ $user->phone_number ?? '--' }}    </span></h5>
                    <h5>CNIC. <span>{{ $user->cnic ?? '--' }}    </span></h5>
                    <h5>Gender <span>{{ ucfirst($user->gender) ?? '--' }}</span></h5>
                    <h5>Age <span>{{ $user->age ?? '--' }}</span></h5>
                    <h5>Address
                        <span>{{ $user->address .', '. $user->area->name .', '. $user->city->name ?? '--' }}</span></h5>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('js')
    <script>
        function readURL(input, id) {

            if (input.files && input.files[0]) {


                var formData = new FormData($('#submit-image')[0]);
                $.ajax({
                    type: "POST",
                    url: "{{ route('staff.profile.edit.image') }}",
                    data: formData,
                    contentType: false,
                    processData: false,
                    success: function (response) {
                        toastr[response.status](response.message);
                        window.location.reload();
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });

                // reader.readAsDataURL(input.files[0]);
                // }
            }
        }
    </script>
@endpush
