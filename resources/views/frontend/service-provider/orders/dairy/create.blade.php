@extends('frontend.layout.app')

@push('css')
    <style></style>

@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Dairy</h3>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection

@section('content')
    <div class="serviceInner">

        <div class="tab-content appointmentData">
            <div id="new" class="tab-pane fade in active">
                <div class="add_diagnosis_wrap">
                    <p class="diagnosis_title">Patient's Diagnosis </p>
                    <div class="diagnosis_form_wrap">
                        <form method="post" action="{{ route('user.dairy.store') }}"
                              enctype="multipart/form-data" class="diagnosis_form">
                            @csrf
                            <input type="hidden" name="user_id" value="{{$id}}">
                            <div class="diagnosis_input">
                                <input id="title" type="text"
                                       class="form-control @error('title') is-invalid @enderror"
                                       name="title" value="{{ old('title') }}"
                                       autocomplete="title" autofocus>
                                @error('title')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                            <div class="diagnosis_textarea">
                                   <textarea name="description" id="description" rows="10" cols="80"
                                             required>{{ old('description') }}</textarea>

                                @error('description')
                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                @enderror
                            </div>
                            <div class="submit_form_btn">
                                <input class="submitBTN" type="submit" value="Submit">
                            </div>
                        </form>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@push('js')
    <script src="https://cdn.ckeditor.com/4.13.1/standard/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('description');
    </script>
@endpush


