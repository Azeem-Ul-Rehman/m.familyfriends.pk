@extends('frontend.layout.app')

@push('css')
    <style></style>

@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Dairy</h3>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection

@section('content')
    <div class="add_new_btn">
        <a href="{{ route('user.dairy.create',$id) }}">Add New</a>
    </div>
    <div class="serviceInner">

        <div class="tab-content appointmentData">
            <div id="new" class="tab-pane fade in active">
                <div class="servicesMain appointments_services">
                    @if(!empty($dairies) && count($dairies) > 0)
                        @foreach($dairies as $dairy)
                            <div class="row">
                                <div class="appointmentRow">
                                    <div class="appointImg">
                                        <div class="appointment_item_img">
                                            <img src="{{ asset('frontend/images/profileImg.png') }}"
                                                 alt="profileImg">
                                        </div>

                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 appointment-short">
                                        <div class="appointRight">
                                            <h4>{{ $dairy->title }}</h4>
                                            <p class="appointment-date">Created By: {{$dairy->name}}</p>
                                            <p class="appointment-date">Date
                                                : {{ date('d-m-Y',strtotime($dairy->created_at)) }}</p>

                                        </div>
                                        <div class="appointLeft">
                                            <div class="viewDate">
                                                <a href="{{ route('user.dairy.show',$dairy->id) }}">View
                                                    Details</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        @endforeach
                    @endif
                </div>

            </div>

        </div>
    </div>
@endsection

@push('js')
@endpush
