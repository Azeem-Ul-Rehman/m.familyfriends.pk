@extends('frontend.layout.app')

@push('css')
    <style></style>

@endpush

@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Dairy</h3>
        </div>
        <div class="clearfix"></div>
    </div>

@endsection

@section('content')
    <div class="serviceInner">

        <div class="tab-content appointmentData">
            <div id="new" class="tab-pane fade in active">
                <div class="diagnosis_wrap">
                    <p class="diagnosis_title">Patient's Diagnosis </p>
                    <div class="diagnosis_box">
                        <div class="diagnosisBoxHeader">
                            <h5 class="diagnosisBoxTitle">{{ $dairy->title }} </h5>
                            <p class="diagnosisPubDate">Date : {{ date('d-m-Y',strtotime($dairy->created_at)) }}</p>
                        </div>
                        <div class="diagnosis_detail">
                            {!! $dairy->description !!}
                        </div>
                        <div class="goback">
                            <a class="goBackBtn" href="{{ URL::previous() }}">Go back</a>
                        </div>
                    </div>
                    <div class="add_new_diagnosis">
                        <a href="{{ route('user.dairy.create',$dairy->user_id) }}">Add New</a>
                    </div>
                </div>

            </div>

        </div>
    </div>
@endsection

@push('js')
@endpush
