@extends('frontend.layout.app')
@push('css')
    <style>
        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }
    </style>
@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            {{--<a href="{{ URL::previous() }}">←</a>--}}
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Home</h3>
        </div>
{{--        <div class="col-md-4 col-sm-4 col-xs-4 text-right">--}}
{{--            <a href="#"><img src="images/profileEdit.png" alt=""></a>--}}
{{--        </div>--}}
        <div class="clearfix"></div>
    </div>
@endsection
@section('content')

<div class="serviceInner">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12 aboutContent">
            <h3>Dashboard</h3>
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="dashboardBoxes box1">
                        <h3>{{ $total_orders }}</h3>
                        <h4>All <span>Appointments</span></h4>
                    </div>
                    <div class="boxFooter box1Footer">
                        <a href="javascript:void(0)" onclick="getordertype('completed')">More info →</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="dashboardBoxes box2">
                        <h3>{{ $pending_orders }}</h3>
                        <h4>New <span>Appointments</span></h4>
                    </div>
                    <div class="boxFooter box2Footer">
                        <a href="javascript:void(0)"  onclick="getordertype('pending')">More info →</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="dashboardBoxes box3">
                        <h3>{{ $accepted_orders->count() }}</h3>
                        <h4>Accepted <span>Appointments</span></h4>
                    </div>
                    <div class="boxFooter box3Footer">
                        <a href="javascript:void(0)"  onclick="getordertype('accepted')">More info →</a>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <div class="dashboardBoxes box4">
                        <h3>{{ $completed_orders->count() }}</h3>
                        <h4>Completed <span>Appointments</span></h4>
                    </div>
                    <div class="boxFooter box4Footer">
                        <a href="javascript:void(0)"  onclick="getordertype('completed')">More info →</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
function getordertype(type){
    $.ajax({
        type: "POST",
        url: "{{ route('staff.store_order_list_type') }}",
        data: {"_token": "{{ csrf_token() }}", type:type},
        dataType: "json",
        cache: true,
        success: function (response) {
            if (response.status == "success") {
                var url = '{{route("staff.appointment.history")}}';
                window.location.href=url;
            }
        },
        error: function () {
            toastr['error']("Something Went Wrong.");
        }
    });
}
</script>
@endpush
