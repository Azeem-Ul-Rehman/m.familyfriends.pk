@extends('frontend.layout.app')

@push('css')
@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Notifications</h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection

@section('content')
    <div class="serviceInner notifications">

        @if(!empty($notifications) && (count($notifications) > 0 ))
            @foreach($notifications as $notification)
                <div class="row form-group">
                    <div class="col-md-2 col-sm-2 col-xs-3">
                        <img src="{{ asset('frontend/images/notification.png') }}" alt="">
                    </div>
                    <div class="col-md-10 col-sm-10 col-xs-9">
                        <p>{{ $notification->message }}</p>
                    </div>
                </div>
            @endforeach
        @endif


    </div>

@endsection

@push('js')
@endpush
