@extends('frontend.layout.app')
@push('css')
<link rel="stylesheet" href="{{ asset('frontend/css/dataTables.bootstrap.min.css') }}">
    <style>
        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }
    </style>
@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Ledger</h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('content')

<div class="serviceInner serviceCatInner">
    <div class="col-md-12 col-sm-12 col-xs-12 appointmentLeft orderReviewLeft ledger">				
    <div class="tableScroll">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="ledger">
            <thead>
            <tr>
                <th> Order No.</th>
                <th> Customer Name</th>
                <th> Service Provider Name</th>
                <th> Completion Date/Time</th>
                <th> Service Provider Comission %</th>
                <th> Grand Total</th>
                <th> Service Provider Share</th>
                <th> Company Share</th>
            </tr>
            </thead>
            <tbody>
                @if(!empty($orders))
                    @foreach($orders as $key=>$order)
                        <tr>
                            <td><a href="{{route('staff.user.appointment.show',$order->id)}}">{{$order->order_id}}</a></td>
                            <td>{{$order->user->fullName()}}</td>
                            <td>{{$order->staff->fullName()}}</td>
                            <td>{{$order->order_end_time}}</td>
                            <td>{{$order->staff->comission->staff_comission}}</td>
                            <td>{{$order->total_price}}</td>
                            <td>{{($order->total_price*$order->staff->comission->staff_comission)/100  }}</td>
                            <td>{{($order->total_price*$order->staff->comission->company_comission)/100  }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
            <tfoot>
            <tr>
                <th colspan="5" style="text-align:right"><b>Total:</b></th>
                <th></th>
                <th></th>
                <th></th>

            </tr>
            </tfoot>
        </table>
    </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection
@push('js')

<script src="{{ asset('frontend/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('frontend/js/dataTables.bootstrap.min.js') }}"></script>
    <script>


        var table = $("#ledger").DataTable({
            "footerCallback": function ( row, data, start, end, display ) {
                var api = this.api(), data;

                // Remove the formatting to get integer data for summation
                var intVal = function ( i ) {
                    return typeof i === 'string' ?
                        i.replace(/[\$,]/g, '')*1 :
                        typeof i === 'number' ?
                            i : 0;
                };

                totalprice = api
                    .column( 5 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                totalprovider = api
                    .column( 6 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                totalcomapny = api
                    .column( 7 )
                    .data()
                    .reduce( function (a, b) {
                        return intVal(a) + intVal(b);
                    }, 0 );

                // Update footer
                $( api.column( 5 ).footer() ).html(
                    totalprice);
                $( api.column( 6 ).footer() ).html(
                    totalprovider);
                $( api.column( 7 ).footer() ).html(
                    totalcomapny);
            },
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });

    </script>

@endpush
