@extends('frontend.layout.app')
@push('css')
<link rel="stylesheet" href="{{ asset('frontend/css/dataTables.bootstrap.min.css') }}">
    <style>
        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }
    </style>
@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Order Wise </h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('content')

<div class="serviceInner serviceCatInner">
    <div class="col-md-12 col-sm-12 col-xs-12 appointmentLeft orderReviewLeft ledger">				
    <div class="tableScroll">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="example">
            <thead>
            <tr>
                <th> Order No.</th>
                <th>  Company Name</th>
                <th>  Grand Total</th>
                <th>  Company Share</th>
                <th>  Given Amount</th>
                <th>  Remianing Amount</th>
            </tr>
            </thead>
            <tbody>
                @if(!empty($Orders))
                    @foreach($Orders as $key=>$order)
                        <tr>
                            <td><a href="{{route('staff.user.appointment.show',$order->id)}}">{{$order->order_id}}</a>
                            </td>
                            <td>Company</td>
                            <td>{{$order->total_price}}</td>
                            <td>{{($order->total_price*$order->staff->comission->company_comission)/100  }}</td>
                            <td>{{ ($order->received_amount - $order->give_amount)  }}</td>
                            <td>{{ ($order->company_commission -($order->received_amount - $order->give_amount))  }}</td>
                        </tr>
                    @endforeach
                @endif
                </tbody>
        </table>
    </div>
    </div>
    <div class="clearfix"></div>
</div>
@endsection
@push('js')
<script src="{{ asset('frontend/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('frontend/js/dataTables.bootstrap.min.js') }}"></script>
<script>
    var table = $("#example").DataTable();

</script>
@endpush

