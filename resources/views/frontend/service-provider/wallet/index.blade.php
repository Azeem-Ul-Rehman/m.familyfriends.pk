@extends('frontend.layout.app')
@push('css')
    <style>
        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }
    </style>
@endpush
@section('header')
    <div class="mobHeader">
        <div class="col-md-4 col-sm-4 col-xs-4">
            <a href="{{ URL::previous() }}">←</a>
        </div>
        <div class="col-md-4 col-sm-4 col-xs-4">
            <h3>Wallet</h3>
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@section('content')

<div class="serviceInner serviceCatInner">
    <div class="col-md-12 col-sm-12 col-xs-12 appointmentLeft orderReviewLeft ledger">
    <div class="row">
        <div class="col-md-6"></div>
        <div class="col-md-6 form-group">
            <a class="btn btn-primary btnMain" data-target="#payment" data-toggle="modal">Payment</a>
        </div>
    </div>
    <div class="form-group">
        <p>Total Recived Amount : <span>{{$provider_received_amount}}</span></p>	
        <p>Total Give Amount : <span>{{$provider_give_amount}}</span></p>
        <p>Wallet : <span>{{$wallet_provider_amount}}</span></p>
    </div>			
    <div class="tableScroll">
        <table class="table table-striped- table-bordered table-hover table-checkable" id="wallet">
            <thead>
            <tr>
                <th>Company Name</th>
                <th>Amount</th>
                <th>Order wise share</th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <td>Company</td>
                    <td>{{ ($company_share_amount - ($received_amount - $give_amount)) < 1 ? 0 : ($company_share_amount - ($received_amount - $give_amount)) }}</td>
                    <td><a href="{{route('staff.get.user.companyOrder',$staff_id)}}">View Order Wise</a></td>
                </tr>
            </tbody>
        </table>
    </div>
    </div>
    <div class="clearfix"></div>
</div>

<div id="payment" class="modal fade" role="dialog">
    <div class="modal-dialog">
  
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title">Payment</h4>
        </div>
        <div class="modal-body">
          <form class="m-form" method="post" action="{{ route('staff.sendpayment') }}" id="create"
          enctype="multipart/form-data" role="form">
          @csrf
          <input type="hidden" id="txtorderid" name="order_id">
              <div class="form-group">
                <label for="total_amount"
                class="col-md-12 col-form-label text-md-left">{{ __('Total Amount') }}</label>
             <input id="total_amount" type="number"
                class="form-control @error('total_amount') is-invalid @enderror"
                name="total_amount"
                autocomplete="total_amount" autofocus min="1" required="">

         @error('total_amount')
         <span class="invalid-feedback" role="alert">
                     <strong>{{ $message }}</strong>
                 </span>
         @enderror
              </div>
              <div class="form-group">
                <label for="payment_mode"
                class="col-md-12 col-form-label text-md-left">{{ __('Payment Mode') }}
             <span class="mandatorySign">*</span></label>

         <select id="payment_mode"
                 class="form-control payment_mode @error('staff_id') is-invalid @enderror"
                 name="payment_mode" autocomplete="payment_mode" required="" onchange="forimagehide()">
             <option value="">Select a Payment Mode</option>
             <option value="Bank">Bank</option>
             <option value="EasyPaisa">EasyPaisa</option>
             <option value="Cash">Cash</option>
         </select>
         @error('payment_mode')
         <span class="invalid-feedback" role="alert">
              <strong>{{ $message }}</strong>
         </span>
         @enderror
              </div>
              <div class="form-group" id="imageshow">
                <label for="deposit_slip"
                class="col-md-6 col-form-label text-md-left">{{ __('Deposit Slip') }}
             <span class="mandatorySign">*</span></label>
         <input value="{{old('deposit_slip')}}" type="file"
                class="form-control @error('deposit_slip') is-invalid @enderror"
                onchange="readURL(this)" id="deposit_slip"
                name="deposit_slip" style="padding: 9px; cursor: pointer" required="">
         <img width="300" height="200" class="img-thumbnail" style="display:none;"
              id="img" src="#"
              alt="your image"/>

         @error('deposit_slip')
         <span class="invalid-feedback" role="alert">
           <strong>{{ $message }}</strong>
         </span>
         @enderror
              </div>
              <div class="row">
                  <div class="col-md-6 col-sm-6 col-xs-6">
                      <a class="btn btn-block btn-primary btnMain" data-dismiss="modal">Close</a>
                  </div>
                  <div class="col-md-6 col-sm-6 col-xs-6">
                    <button type="submit" class="btn btn-block btn-primary btnMain">
                        {{ __('SAVE') }}
                    </button>
                  </div>
              </div>
          </form>
        </div>
      </div>
  
    </div>
  </div>

@endsection
@push('js')
 <script>

        function forimagehide(){
            var value = $("#payment_mode option:selected").val();
            if(value == "Cash"){
                $("#imageshow").attr('hidden','hidden');
                $("#deposit_slip").removeAttr('required');
            }
            else{
             $("#imageshow").removeAttr('hidden');
             $("#deposit_slip").attr('required','required');
            }
        }

    </script>
@endpush
