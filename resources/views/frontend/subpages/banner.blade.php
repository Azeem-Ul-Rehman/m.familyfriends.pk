@php
$services = \App\Models\ServiceCategory::take(3)->get();

@endphp

<div id="myCarousel" class="carousel slide carousel-fade" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
        <li data-target="#myCarousel" data-slide-to="1"></li>
        <li data-target="#myCarousel" data-slide-to="2"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">
        <div class="item active itemBg"
             style="background: url({{ asset('frontend/images/banner.png') }}) no-repeat; background-size: cover; background-position: center;">
            <div class="container">
                <div class="carousel-caption">
                    <h3>{{ $services[0]->name }}</h3>
                    <p>{{\Illuminate\Support\Str::limit($services[0]->description,150)}}</p>
                    <a href="#" class="btn btn-primary btnMain">View More</a>
                </div>
            </div>
        </div>
        <div class="item itemBg"
             style="background: url({{ asset('frontend/images/banner.png') }}) no-repeat; background-size: cover; background-position: center;">
            <div class="container">
                <div class="carousel-caption">
                    <h3>{{ $services[0]->name }}</h3>
                    <p>{{\Illuminate\Support\Str::limit($services[0]->description,150)}}</p>
                    <a href="#" class="btn btn-primary btnMain">View More</a>
                </div>
            </div>
        </div>
        <div class="item itemBg"
             style="background: url({{ asset('frontend/images/banner.png') }}) no-repeat; background-size: cover; background-position: center;">
            <div class="container">
                <div class="carousel-caption">
                    <h3>{{ $services[0]->name }}</h3>
                    <p>{{\Illuminate\Support\Str::limit($services[0]->description,150)}}</p>
                    <a href="#" class="btn btn-primary btnMain">View More</a>
                </div>
            </div>
        </div>
    </div>

</div>
