
<section class="cta">
    <div class="container">
        <div class="text-center">
            <div class="col-md-6 col-md-offset-6 col-sm-offset-6 col-sm-6 col-xs-12 ctaDet">
                <img src="{{ asset('frontend/images/ctaLogo.png') }}" alt="">
                <h5>For better experience download our app</h5>
                <a href="#"><img src="{{ asset('frontend/images/appStore.png') }}" alt=""></a>
                <a href="#"><img src="{{ asset('frontend/images/googlePlay.png') }}" alt=""></a>
            </div>
        </div>
    </div>
</section>
