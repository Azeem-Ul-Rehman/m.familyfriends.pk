@extends('service-provider.main')
@section('title','Appointments')
@push('css')
    <style>

        /*.btn-height {*/
        /*height: 40px;*/
        /*}*/
        @media screen and (max-width: 650px) {
            .responsiveTable {
                overflow-x: scroll;
            }

            .responsiveTable a {
                width: 100%;
                margin: 5px 0;
            }

        }

        @media screen and (max-width: 450px) {
            .qr-copy {
                width: 100% !important;
            }

            .input-group-addon {
                width: 100% !important;
                margin-top: 10px;
            }

            .buttonMain {
                width: 100%;
            }
        }
    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Appointments History
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <span>
                                <span><strong>No of Appointments: {{ $orders->count() }}</strong> </span>
                            </span>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">

                <form action="{{ route('staff.appointment.history') }}" method="GET" enctype="multipart/form-data">
                    <div class="form-group row">
                        <div class="col-md-12">
                            <h4>Filter</h4>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xl-3">


                            <label for="start_date" class="col-md-12 col-form-label text-md-left"><strong>Start
                                    Date:</strong></label>

                            <input type="date" class="form-control" id="start_date" name="start_date"
                                   value="{{ old('start_date') }}">


                        </div>
                        <div class="col-md-3 col-sm-3 col-xl-3">
                            <label for="end_date" class="col-md-12 col-form-label text-md-left"><strong>End
                                    Date:</strong></label>
                            <input type="date" class="form-control" id="end_date" name="end_date"
                                   value="{{ old('end_date') }}">
                        </div>
                        <div class="col-md-3 col-sm-3 col-xl-3">

                            <label for="status"
                                   class="col-md-4 col-form-label text-md-left"><strong>Status:</strong></label>

                            <select id="status"
                                    class="form-control @error('status') is-invalid @enderror"
                                    name="status" autocomplete="status">
                                <option value="">Select an option</option>
                                <option value="pending" {{ (request()->get('status') == 'pending') ? 'selected' : '' }}>
                                    Pending
                                </option>
                                <option
                                    value="accepted" {{ (request()->get('status') == 'accepted') ? 'selected' : '' }}>
                                    Accepted
                                </option>
                                <option
                                    value="completed" {{ (request()->get('status') == 'completed') ? 'selected' : '' }}>
                                    Completed
                                </option>
                            </select>

                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <div class="m-form__actions m-form__actions">
                                <label style="display: block"
                                       class="col-md-4 col-form-label text-md-left">&nbsp;</label>
                                <a href="{{ route('staff.appointment.history') }}"
                                   class="btn btn-accent m-btn m-btn--icon m-btn--air refreshBtn">
                                <span>
                                    <i class="la la-refresh"></i>

                                </span>
                                </a>
                                <button class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                    Submit
                                </button>
                            </div>
                        </div>

                    </div>


                </form>
                <hr>
                <div class="responsiveTable">
                    <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                        <thead>
                        <tr>

                            <th> Appointment ID.</th>
                            <th> Requested DateTime</th>
                            <th> Total Price</th>
                            <th> Grand Total</th>
                            <th> Mobile Number</th>
                            <th> Order Status</th>
                            <th> Staff Status</th>
                            <th width="20%"> Action</th>
                        </tr>
                        </thead>
                        <tbody>
                        @if(!empty($orders))
                            @foreach($orders as $key=>$order)


                                <tr>
                                    <td>{{$order->order_id}}</td>
                                    <td>{{\Carbon\Carbon::parse($order->order_details[0]->date)->setTimezone($order->time_zone)->format('m/d/Y')}}  {{\Carbon\Carbon::parse($order->order_details[0]->time)->format('h:i A')}}</td>
                                    <td>{{(int)$order->total_price ?? 0}}</td>
                                    <td>{{(int)$order->total_price + (int)$order->delivery_charges ?? 0}}</td>
                                    <td>{{$order->phone_number ?? '-'}}</td>
                                    <td>{{ucfirst($order->order_status) ?? '-'}}</td>
                                    <td>{{ucfirst($order->staff_status) ?? '-'}}</td>
                                    <td nowrap>
                                        @if($order->staff_status == 'pending')
                                            <a href="javascript:void(0)"
                                               onclick="orderAccepted('{{$order->id}}','accepted')"
                                               class="btn btn-sm btn-info pull-left ">Accept</a>
                                        @elseif($order->staff_status == 'accepted' && is_null($order->order_progress_status))
                                            <a href="javascript:void(0)"
                                               onclick="orderProgress('{{$order->id}}','on-my-way')"
                                               class="btn btn-sm btn-info pull-left ">On my Way</a>
                                        @elseif($order->order_progress_status == 'on-my-way')
                                            <a href="javascript:void(0)"
                                               onclick="orderProgress('{{$order->id}}','start')"
                                               class="btn btn-sm btn-info pull-left ">Start</a>
                                        @elseif($order->order_progress_status == 'start')
                                            <a href="javascript:void(0)"
                                               onclick="orderProgress('{{$order->id}}','end')"
                                               class="btn btn-sm btn-info pull-left ">End</a>
                                       {{-- @elseif($order->order_progress_status == 'end')
                                            <a href="javascript:void(0)"
                                               onclick="orderProgress('{{$order->id}}','collect-cash')"
                                               class="btn btn-sm  btn-info pull-left ">Collect Cash</a>
                                        --}}@endif
                                        <a href="{{route('staff.user.appointment.show',$order->id)}}"
                                           class="btn btn-sm  btn-primary pull-left  ">View</a>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('models')

    <div id="trackEmployee" class="modal fade mw-100 w-75" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Track Employee</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>

                </div>
                <div class="modal-body">
                    <div class="row">

                        <div class="col-lg-12 col-xs-12 col-sm-12">

                            <div id="address-map-container" style="width:100%;height:400px; ">
                                <div style="width: 100%; height: 100%" id="map"></div>
                            </div>

                        </div>


                    </div>
                </div>

            </div>

        </div>
    </div>
@endpush
@push('js')
    <script>
        $("#m_table_1").dataTable({
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [6]}
            ],
        });
    </script>

    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAZQiMEU1xYMEVTgch8O5WmL-iZVfQjko0&libraries=places"
        async defer></script>

    <script type="text/javascript">
        var locations = [];

        function getLatLng(id) {
            var order_id = parseInt(id);
            var request = {"order_id": order_id};
            if (order_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.get.latlng') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {


                            var staff_image = '/uploads/pointers/staff_pointer.png';
                            var customer_image = '/uploads/pointers/customer_pointer.png';
                            locations.push([response.data.staff_latlng.address, response.data.staff_latlng.latitude, response.data.staff_latlng.longitude, response.data.staff_latlng.first_name, response.data.staff_latlng.last_name, response.data.staff_latlng.profile_pic, staff_image]);
                            locations.push([response.data.customer_latlng.address, response.data.customer_latlng.latitude, response.data.customer_latlng.longitude, response.data.customer_latlng.first_name, response.data.customer_latlng.last_name, response.data.customer_latlng.profile_pic, customer_image]);
                            initialize();

                            $('#trackEmployee').modal('show');

                            {{--toastr['success']("Staff Assigned Successfully");--}}

                            {{--setTimeout(function () {--}}
                            {{--    window.location = '{{route('admin.get.order.history')}}';--}}
                            {{--}, 1500);--}}
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please Select Staff");
            }
        }

        function initialize() {


            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 12,
                center: new google.maps.LatLng(31.5204, 74.3587),
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    icon: locations[i][6]
                });

                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                    return function () {
                        var contentString = '';

                        contentString += '<div id="content">';
                        contentString += '<div id="siteNotice">';
                        contentString += '<img id="image" name="image" src="/uploads/user_profiles/' + locations[i][5] + '"  class="map-image"/>';
                        contentString += '<h5 id="firstHeading" class="firstHeading">' + locations[i][3] + ' ' + locations[i][4] + '</h5>';//doesnt work here
                        contentString += '<div id="bodyContent">' + locations[i][0] + '</div>';
                        contentString += '</div>';

                        infowindow.setContent(contentString);
                        // infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }
        }


        function orderAccepted(orderId, orderStaffStatus) {
            var order_id = parseInt(orderId);
            var request = {
                "order_id": order_id,
                "order_staff_status": orderStaffStatus,
                "_token": "{{ csrf_token() }}"
            };
            if (order_id !== '') {
                $.ajax({
                    type: "POST",
                    url: "{{ route('staff.update.order.staff.status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            toastr[response.status](response.message);
                            setTimeout(function () {
                                window.location.reload();
                            },2000);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }

        function orderProgress(orderId, orderProgressStatus) {
            var order_id = parseInt(orderId);
            var request = {
                "order_id": order_id,
                "order_progress_status": orderProgressStatus,
                "_token": "{{ csrf_token() }}"
            };
            if (order_id !== '') {
                $.ajax({
                    type: "POST",
                    url: "{{ route('staff.update.order.progress.status') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            toastr[response.status](response.message);
                            if(orderProgressStatus == "end"){
                                var url = '{{route("staff.user.appointment.show", ":id")}}';
                                url = url.replace(':id', order_id);
                                window.location.href=url;
                            }
                            else{
                            setTimeout(function () {
                                window.location.reload();
                            },2000);
                         }
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            }
        }

    </script>
@endpush
