@extends('service-provider.main')
@section('title','Wallet')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Wallet
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                {{--tableScroll--}}

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th> Order No.</th>
                        <th> Company Name</th>
                        <th> Grand Total</th>
                        <th> Company Comission %</th>
                        <th> Company Share</th>
                        <th> Given Amount</th>
                        <th> Remianing Amount</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($Orders))
                        @foreach($Orders as $key=>$order)
                            <tr>
                                <td><a href="{{route('staff.user.appointment.show',$order->id)}}">{{$order->order_id}}</a>
                                </td>
                                <td>Company</td>
                                <td>{{$order->total_price}}</td>
                                <td>{{$order->staff->comission->company_comission}}</td>
                                <td>{{($order->total_price*$order->staff->comission->company_comission)/100  }}</td>
                                <td>{{ ($order->received_amount - $order->give_amount)  }}</td>
                                <td>{{ ($order->company_commission -($order->received_amount - $order->give_amount))  }}</td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });

    </script>

@endpush
