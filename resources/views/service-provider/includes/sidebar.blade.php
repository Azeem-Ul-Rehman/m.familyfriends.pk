<button class="m-aside-left-close  m-aside-left-close--skin-dark " id="m_aside_left_close_btn"><i
        class="la la-close"></i></button>
<div id="m_aside_left" class="m-grid__item	m-aside-left  m-aside-left--skin-dark ">

    <!-- BEGIN: Aside Menu -->
    <div id="m_ver_menu" class="m-aside-menu  m-aside-menu--skin-dark m-aside-menu--submenu-skin-dark"
         m-menu-vertical="1" m-menu-scrollable="1" m-menu-dropdown-timeout="500" style="position: relative;">
        <ul class="m-menu__nav  m-menu__nav--dropdown-submenu-arrow ">

            <li class="m-menu__item "
                aria-haspopup="true"><a href="{{ route('index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-home"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Home</span>
											</span></span></a></li>
            <li class="m-menu__item {{ (request()->is('staff/dashboard')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('staff.dashboard.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-dashboard"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Dashboard</span>
											</span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('staff/profile') || request()->is('staff/edit') || request()->is('staff/edit/*') || request()->is('staff/update/profile/*') || request()->is('staff/update') || request()->is('staff/update/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('staff.profile.index') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-profile-1"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Profile</span>
											</span></span></a></li>


            <li class="m-menu__item  {{ (request()->is('staff/appointment-history') || request()->is('staff/appointment/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('staff.appointment.history') }}" class="m-menu__link "><i
                        class="m-menu__link-icon flaticon-time"></i><span
                        class="m-menu__link-title"> <span
                            class="m-menu__link-wrap"> <span class="m-menu__link-text">Appointments History</span>
                                            </span></span></a></li>
            <li class="m-menu__item  {{ (request()->is('staff/ledger') || request()->is('staff/ledger/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('staff.ledger.index') }}" class="m-menu__link "><i
                   class="m-menu__link-icon flaticon-information"></i><span
                     class="m-menu__link-title"> <span
                       class="m-menu__link-wrap"> <span class="m-menu__link-text">Ledger</span>
            </span></span></a></li>

            <li class="m-menu__item  {{ (request()->is('staff/wallet') || request()->is('staff/wallet/*')) ? 'activeMenu' : '' }} "
                aria-haspopup="true"><a href="{{ route('staff.wallet.index') }}" class="m-menu__link "><i
                   class="m-menu__link-icon flaticon-information"></i><span
                     class="m-menu__link-title"> <span
                       class="m-menu__link-wrap"> <span class="m-menu__link-text">Wallet</span>
            </span></span></a></li>

        </ul>
    </div>

    <!-- END: Aside Menu -->
</div>
