<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
@include('frontend.include.meta-tags')

<!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <!-- Bootstrap -->
    <link href="{{ asset('frontend/css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('frontend/css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/media.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700;900&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700;900&display=swap"
          rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{--Date Time Picker--}}

    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('frontend/css/select2.min.css') }}"/>


</head>
<body>

<div class="mobileContainer">
    <div class="signIn mobileContentArea">
        <div class="innerContainer">
            <div class="backLink">
            <a href="{{ route('service') }}"><img src="{{ asset('frontend/images/arrowBack.png') }}" alt=""></a>
            </div>
            <div class="logo"><a href="{{ route('index') }}"><img src="{{ asset('frontend/images/logo.png') }}" alt=""></a></div>
            <div class="fieldsArea">
                <h4>Login</h4>
                <form method="POST" action="{{ route('login') }}" autocomplete="off">
                    @csrf
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Enter your Phone No</label>
                                <input type="text" class="form-control @error('phone_number') is-invalid @enderror"
                                       name="phone_number" id="phone_number" value="{{ old('phone_number') }}"
                                       maxlength="11" autocomplete="phone_number"
                                       placeholder="03001234567" pattern="[03]{2}[0-9]{9}"
                                       onkeyup="if (/\D/g.test(this.value)) this.value = this.value.replace(/\D/g,'')"
                                       title="Phone number with 03 and remaing 9 digit with 0-9"
                                       autofocus>
                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Enter your Password</label>
                                <input id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btnMain btnDetails">Sign in</button>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12 forgotPass">
                            @if (Route::has('password.request'))
                                <p><a href="{{ route('password.request') }}">{{ __('Forgot Password?') }}</a></p>
                            @endif
                        </div>
                        <div class="clearfix"></div>
                        <div class="signInOptions">
                            <div class="col-md-12">
                                <h4>Don't have an account? Sign Up</h4>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <a href="{{ route('register') }}" class="btn btn-primary btnMain btnDetails">As Customer</a>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-6">
                                <a href="{{ route('register.family.friend') }}" class="btn btn-primary btnMain btnDetails">As Staff</a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="signInBtm">
                <div class="row">

                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p>By signing in or creating an account, you agree with our <a href="#">Terms & Conditions</a>
                            and <a href="#">Privacy Statement</a></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('frontend/js/jquery-1.11.3.min.js') }}"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('frontend/js/bootstrap.js') }}"></script>
<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>

<script src="{{ asset('frontend/js/custom.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('frontend/js/toastr.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquerysession.js') }}"></script>

<script type="text/javascript" src="{{ asset('frontend/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

<script type="text/javascript" src="{{asset('frontend/js/select2.min.js')}}"></script>


<script>
    $.ajaxSetup({
        headers: {
            'csrftoken': '{{ csrf_token() }}',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ajaxStart(function () {
        $('#loader').show();
    });

    $(document).ajaxStop(function () {
        $('#loader').hide();
    });

    $('form').submit(function () {
        $('.text-danger').removeClass('text-danger');
        $('.is-invalid').removeClass('is-invalid');
        $('small.text-danger').remove();
    });

    function ajaxResponseHandler(response, form) {
        if (response.status == "success" || response.status == '200') {
            $(form).parent().find('.text-danger').removeClass('text-danger');
            $(form).parent().find('.is-invalid').removeClass('is-invalid');
            $(form).parent().find('small.ajax-error-text').remove();
            if (response.message != '')
                toastr['success'](response.message);
        } else {
            $(form).parent().find('.text-danger').removeClass('text-danger');
            $(form).parent().find('.is-invalid').removeClass('is-invalid');
            $(form).parent().find('small.ajax-error-text').remove();
            if (response.message != '')
                toastr['error'](response.message);
            console.log(response.data);
            $.each(response.data, function (elm, errors) {
                $.each(errors, function (k, v) {
                    var splited = elm.split('.');
                    if (splited.length > 1) {
                        if (parseInt(splited[1]) >= 0 && typeof splited[2] != 'undefined') {
                            var node_index = parseInt(splited[1]);
                            var node = $(form).find('[name="' + splited[0] + '[][' + splited[2] + ']"]')[node_index];
                        } else if (parseInt(splited[2]) >= 0) {
                            var node_index = parseInt(splited[2]);
                            var node = $(form).find('[name="' + splited[0] + '[' + splited[1] + '][]"]')[node_index];
                        } else {
                            var node_index = parseInt(splited[1]);
                            var node = $(form).find('[name="' + splited[0] + '[]"]')[node_index];
                        }
                    } else {
                        var node = $(form).find('[name="' + elm + '"]');
                    }
                    $(node).prev().addClass('text-danger');
                    $(node).addClass('is-invalid');
                    $(node).find('small.ajax-error-text').remove();
                    debugger;

                    // $('.invalid-feedback-phone-number').text(v);
                    // $('.invalid-feedback-phone-number').show();

                    $(node).parent().append('<small class="text-danger ajax-error-text">' + v + '</small>');
                });
            });
        }
    }


    function successResponseHandler(response) {
        if (response.status == "success" || response.status == '200') {
            if (response.message != '')
                toastr['success'](response.message);
        } else {
            if (response.message != '')
                toastr['error'](response.message);
        }
    }

    function sortMe(element) {
        var options = $(element);
        var arr = options.map(function (_, o) {
            return {t: $(o).text(), v: o.value};
        }).get();
        arr.sort(function (o1, o2) {
            return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
        });
        options.each(function (i, o) {
            o.value = arr[i].v;
            $(o).text(arr[i].t);
        });
    }


    function checkCookie() {

        var check_cookie = "{{\Illuminate\Support\Facades\Cookie::has('referral_code')}}";
        if (check_cookie) {
            var get_cookie = "{{\Illuminate\Support\Facades\Cookie::get('referral_code')}}"
            $('#referred_by').val(get_cookie);
            $('#referral-code-style').css('display', '');
        }
    }

    setTimeout(function () {
        checkCookie();
    }, 2000);

        @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif

</script>

<script>
    $(document).ready(function () {
        $('nav ul li a').click(function () {
            $('li a').removeClass("active");
            $(this).addClass("active");
        });

        if ($('.cnic-mask').length) {
            $('.cnic-mask').mask('00000-0000000-0');
        }
        if ($('.phone_number').length) {
            $('.phone_number').mask('00000000000');
        }

    });
</script>


<script>
        @if(Session::has('user'))

    var type = "{{ Session::get('user') }}";
    if (type == 'register') {
        $('#thankYouModal').modal('show');
        $('.modal').css('display', 'block');
    }
    @endif

    $('#modelClose').click(function () {
        $('#thankYouModal').modal('hide');
        $('.modal').css('display', 'none');
        var forgetSession = "{{   session()->forget('user') }}";
    });

</script>

<script>

    $('#phone_number').focusout(function () {
        if (/^(03)\d{9}$/.test($(this).val())) {
            // value is ok, use it
        } else {

        }
    });
</script>

</body>
</html>

