<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
@include('frontend.include.meta-tags')

<!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('favicon.png') }}">
    <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}">
    <!-- Bootstrap -->
    <link href="{{ asset('frontend/css/bootstrap.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

    <!-- Toastr -->
    <link rel="stylesheet" href="{{ asset('frontend/css/toastr.min.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/custom.css') }}">
    <link rel="stylesheet" href="{{ asset('frontend/css/media.css') }}">

    <link href="https://fonts.googleapis.com/css2?family=Source+Sans+Pro:wght@300;400;600;700;900&display=swap"
          rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500;600;700;900&display=swap"
          rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    {{--Date Time Picker--}}

    <link rel="stylesheet" href="{{ asset('frontend/css/bootstrap-datetimepicker.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('frontend/css/select2.min.css') }}"/>
    <style>
        /*.input-codes {*/
        /*    width: 24%;*/
        /*    height: 45px;*/
        /*    text-align: center;*/
        /*    font-weight: 500;*/
        /*    font-size: 22px;*/
        /*}*/
    </style>

</head>
<body>
<div class="mobileContainer">
    <div class="signIn mobileContentArea">
        <div class="innerContainer">
            <div class="logo"><a href="{{ route('index') }}"><img src="{{ asset('frontend/images/logo.png') }}" alt=""></a>
            </div>
            <div class="fieldsArea">
                @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session('status') }}
                    </div>
                @endif
                <h4>We'll help you if you forget your password</h4>
                <form method="POST" action="javascript:void(0)" id="forget-password">
                    @csrf
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="phone_number">{{ __('Phone Number') }}<span>*</span></label>
                                <input id="phone_number" type="tel"
                                       class="form-control @error('phone_number') is-invalid @enderror"
                                       name="phone_number" value="{{ old('phone_number') }}" required
                                       autocomplete="phone_number" pattern="[03]{2}[0-9]{9}"
                                       title="Phone number with 03 and remaing 9 digit with 0-9"
                                       autofocus>
                                @error('phone_number')
                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary btnMain btnDetails">Next</button>
                            </div>
                        </div>
                    </div>
                </form>
                <form method="POST" action="javascript:void(0)" id="otp-send" style="display: none">
                    <input type="hidden" value="0" name="mobile_number" id="mobile_number">
                    @csrf
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label>Enter your 4-digit code you receive via OTP</label>
                                <input class="form-control otpField" type="text" maxLength="1" size="1" min="0" max="9"
                                       pattern="[0-9]{1}" id="otp_code1" name="otp_code1"/>
                                <input class="form-control otpField" type="text" maxLength="1" size="1" min="0" max="9"
                                       pattern="[0-9]{1}" id="otp_code2" name="otp_code2"/>
                                <input class="form-control otpField" type="text" maxLength="1" size="1" min="0" max="9"
                                       pattern="[0-9]{1}" id="otp_code3" name="otp_code3"/>
                                <input class="form-control otpField" type="text" maxLength="1" size="1" min="0" max="9"
                                       pattern="[0-9]{1}" id="otp_code4" name="otp_code4"/>
                                <input id="otp_code" type="hidden"
                                       class="form-control @error('otp_code') is-invalid @enderror"
                                       name="otp_code" value="{{ old('otp_code') }}" required
                                       autocomplete="otp_code" autofocus>
                                @error('otp_code')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <button type="submit"
                                        class="btn btn-primary btnMain btnDetails">{{ __('Verify') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
                <form method="POST" action="javascript:void(0)" id="password-reset" style="display: none">
                    @csrf
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group ">
                            <input type="hidden" value="" name="mobile_number" id="mobile_number_forget">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="password">{{ __('Password') }}<span>*</span></label>
                                <input id="password" type="password"
                                       class="form-control @error('password') is-invalid @enderror" name="password"
                                       required
                                       autocomplete="new-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <label for="password-confirm">{{ __('Confirm Password') }}<span>*</span></label>
                                <input id="password-confirm" type="password" class="form-control"
                                       name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 col-xs-12">
                            <div class="form-group">
                                <button type="submit"
                                        class="btn btn-primary btnMain btnDetails">{{ __('Reset Password') }}</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="signInBtm">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h5>Don't have an account? <a href="{{ route('register') }}">Sign Up</a></h5>
                    </div>
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <p>By signing in or creating an account, you agree with our <a href="#">Terms &amp;
                                Conditions</a> and <a href="#">Privacy Statement</a></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{ asset('frontend/js/jquery-1.11.3.min.js') }}"></script>

<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{ asset('frontend/js/bootstrap.js') }}"></script>
<script src="{{ asset('frontend/js/owl.carousel.min.js') }}"></script>

<script src="{{ asset('frontend/js/custom.js') }}"></script>
<!-- Toastr -->
<script src="{{ asset('frontend/js/toastr.min.js') }}"></script>
<script src="{{ asset('frontend/js/jquerysession.js') }}"></script>

<script type="text/javascript" src="{{ asset('frontend/js/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('frontend/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.16/jquery.mask.min.js"></script>

<script type="text/javascript" src="{{asset('frontend/js/select2.min.js')}}"></script>


<script>
    $.ajaxSetup({
        headers: {
            'csrftoken': '{{ csrf_token() }}',
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $(document).ajaxStart(function () {
        $('#loader').show();
    });

    $(document).ajaxStop(function () {
        $('#loader').hide();
    });

    $('form').submit(function () {
        $('.text-danger').removeClass('text-danger');
        $('.is-invalid').removeClass('is-invalid');
        $('small.text-danger').remove();
    });

    function ajaxResponseHandler(response, form) {
        if (response.status == "success" || response.status == '200') {
            $(form).parent().find('.text-danger').removeClass('text-danger');
            $(form).parent().find('.is-invalid').removeClass('is-invalid');
            $(form).parent().find('small.ajax-error-text').remove();
            if (response.message != '')
                toastr['success'](response.message);
        } else {
            $(form).parent().find('.text-danger').removeClass('text-danger');
            $(form).parent().find('.is-invalid').removeClass('is-invalid');
            $(form).parent().find('small.ajax-error-text').remove();
            if (response.message != '')
                toastr['error'](response.message);
            console.log(response.data);
            $.each(response.data, function (elm, errors) {
                $.each(errors, function (k, v) {
                    var splited = elm.split('.');
                    if (splited.length > 1) {
                        if (parseInt(splited[1]) >= 0 && typeof splited[2] != 'undefined') {
                            var node_index = parseInt(splited[1]);
                            var node = $(form).find('[name="' + splited[0] + '[][' + splited[2] + ']"]')[node_index];
                        } else if (parseInt(splited[2]) >= 0) {
                            var node_index = parseInt(splited[2]);
                            var node = $(form).find('[name="' + splited[0] + '[' + splited[1] + '][]"]')[node_index];
                        } else {
                            var node_index = parseInt(splited[1]);
                            var node = $(form).find('[name="' + splited[0] + '[]"]')[node_index];
                        }
                    } else {
                        var node = $(form).find('[name="' + elm + '"]');
                    }
                    $(node).prev().addClass('text-danger');
                    $(node).addClass('is-invalid');
                    $(node).find('small.ajax-error-text').remove();
                    debugger;

                    // $('.invalid-feedback-phone-number').text(v);
                    // $('.invalid-feedback-phone-number').show();

                    $(node).parent().append('<small class="text-danger ajax-error-text">' + v + '</small>');
                });
            });
        }
    }


    function successResponseHandler(response) {
        if (response.status == "success" || response.status == '200') {
            if (response.message != '')
                toastr['success'](response.message);
        } else {
            if (response.message != '')
                toastr['error'](response.message);
        }
    }

    function sortMe(element) {
        var options = $(element);
        var arr = options.map(function (_, o) {
            return {t: $(o).text(), v: o.value};
        }).get();
        arr.sort(function (o1, o2) {
            return o1.t > o2.t ? 1 : o1.t < o2.t ? -1 : 0;
        });
        options.each(function (i, o) {
            o.value = arr[i].v;
            $(o).text(arr[i].t);
        });
    }


        @if(Session::has('flash_message'))
    var type = "{{ Session::get('flash_status') }}";
    switch (type) {
        case 'info':
            toastr.info("{{ Session::get('flash_message') }}");
            break;

        case 'warning':
            toastr.warning("{{ Session::get('flash_message') }}");
            break;

        case 'success':
            toastr.success("{{ Session::get('flash_message') }}");
            break;

        case 'error':
            toastr.error("{{ Session::get('flash_message') }}");
            break;
    }
    @endif

</script>

<script>
    $(document).ready(function () {
        $('nav ul li a').click(function () {
            $('li a').removeClass("active");
            $(this).addClass("active");
        });

        if ($('.cnic-mask').length) {
            $('.cnic-mask').mask('00000-0000000-0');
        }
        if ($('.phone_number').length) {
            $('.phone_number').mask('00000000000');
        }

    });
</script>


<script>
        @if(Session::has('user'))

    var type = "{{ Session::get('user') }}";
    if (type == 'register') {
        $('#thankYouModal').modal('show');
        $('.modal').css('display', 'block');
    }
    @endif

    $('#modelClose').click(function () {
        $('#thankYouModal').modal('hide');
        $('.modal').css('display', 'none');
        var forgetSession = "{{   session()->forget('user') }}";
    });

</script>

<script>

    $('#phone_number').focusout(function () {
        if (/^(03)\d{9}$/.test($(this).val())) {
            // value is ok, use it
        } else {

        }
    });
</script>

<script>

    $(function () {
        'use strict';

        var body = $('body');

        function goToNextInput(e) {
            var key = e.which,
                t = $(e.target),
                sib = t.next('input');
            $(this).val($(this).val().replace(/[^\d].+/, ""));
            if ((e.which < 48 || e.which > 57)) {
                e.preventDefault();
                return false;
            }

            sib.select().focus();
        }


        function onFocus(e) {
            $(e.target).select();
        }

        body.on('keypress keyup blur', 'input[name=otp_code1]', goToNextInput);
        body.on('keypress keyup blur', 'input[name=otp_code2]', goToNextInput);
        body.on('keypress keyup blur', 'input[name=otp_code3]', goToNextInput);
        body.on('keypress keyup blur', 'input[name=otp_code4]', goToNextInput);
        body.on('click', 'input', onFocus);

    });


    $('#forget-password').submit(function (e) {
        $('#mobile_number').val($('#phone_number').val());
        e.preventDefault();
        let form = $(this);
        var request = $(form).serialize();
        $.ajax({
            type: "POST",
            url: "{{ route('otp.send') }}",
            data: request,
            dataType: "json",
            cache: false,
            success: function (response) {
                toastr['success'](response.message);


                $('#forget-password').css('display', 'none');
                $('#otp-send').css('display', 'block');

            },
            error: function () {
                toastr['error']("Something Went Wrong.");
            }
        });

    });

    $('#otp-send').submit(function (e) {
        $('#mobile_number_forget').val($('#mobile_number').val());

        $('#otp_code').val($('#otp_code1').val() + '' + $('#otp_code2').val() + '' + $('#otp_code3').val() + '' + $('#otp_code4').val());

        e.preventDefault();
        let form = $(this);
        var request = $(form).serialize();
        $.ajax({
            type: "POST",
            url: "{{ route('verify.otp') }}",
            data: request,
            dataType: "json",
            cache: false,
            success: function (response) {
                if (response.status == 'success') {
                    $('#forget-password').css('display', 'none');
                    $('#otp-send').css('display', 'none');
                    $('#password-reset').css('display', 'block');
                } else {
                    toastr['error'](response.message);
                }


            },
            error: function (e) {

                toastr['error'](e.responseJSON.error.otp_code);
            }
        });

    });

    $('#password-reset').submit(function (e) {
        $('#mobile_number_forget').val($('#mobile_number').val());

        $('#otp_code').val($('#otp_code1').val() + '' + $('#otp_code2').val() + '' + $('#otp_code3').val() + '' + $('#otp_code4').val());

        e.preventDefault();
        let form = $(this);
        var request = $(form).serialize();
        $.ajax({
            type: "POST",
            url: "{{ route('otp.password.reset') }}",
            data: request,
            dataType: "json",
            cache: false,
            success: function (response) {
                debugger;

                if (response.status == 'success') {
                    toastr['success'](response.message);
                    setTimeout(function () {
                        window.location.href = '{{ route('login') }}';
                    }, 2000)
                } else {
                    toastr[response.status](response.message);
                }


            },
            error: function (e) {
                $.each(e.responseJSON.error, function (index, object) {
                    toastr['error'](object);
                });


            }
        });

    });
</script>


</body>
</html>


