@extends('layouts.master')
@section('title','Orders')
@push('css')
    <style rel="stylesheet">
        .menuItemsCssClass input {
            width: 7%;
            float: left;
            margin-bottom: 15px;
        }

        .menuItemsCssClass span {
            padding-left: 20px;
            /*padding-top: 10px;*/
            display: block;
            margin-bottom: 15px;
        }

        .disabledDiv {
            pointer-events: none;
            opacity: 1.4;
        }

        .card-header a {
            text-decoration: none;
        }

        .accordion .card:first-of-type {
            border: 1px solid rgba(0, 0, 0, 0.125);
        }

        .card-header {
            background: #ff6c2b !important;
        }

        .mb-0 {
            color: #fff !important;
        }

        .orderModal .modal-header {
            background: #ff6c2b !important;
        }

        .orderModal .modal-title {
            color: #fff !important;
        }

        .removeBtn {
            float: right;
        }
    </style>
@endpush
@section('content')

    @php
        $order_detail_menu_item_sum =0;
    @endphp


    <div class="m-content">
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Edit {{ __('Order') }}
                        </h3>
                    </div>
                </div>
            </div>

            <div class="m-portlet__body">
                <div class="col-lg-12">
                    <div class="m-portlet">
                        <form class="m-form" method="post" action="{{ route('admin.order.update') }}" id="add-orders"
                              enctype="multipart/form-data" role="form">
                            @csrf
                            <div class="m-portlet__body">
                                <div class="m-form__section m-form__section--first">
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <p><strong>Order Information</strong></p>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <input type="hidden" value="{{$order->id}}" name="order_id">
                                        <input type="hidden" value="{{$order->payment_status}}" name="payment_status" id="payment_status">
                                        <input type="hidden" value="{{(int)$order->order_details->sum('amount')}}"
                                               name="grand_total"
                                               id="grand_total_price">
                                        <input type="hidden" value="" name="discount" id="discount_order">
                                        <input type="hidden" value="{{(int)$order->delivery_charges}}"
                                               name="delivery_charges" class="delivery-charges">
                                        <input type="hidden" value="0" name="time_zone" id="{{$order->time_zone}}">


                                        <div id="finalService">
                                            <input type="hidden"
                                                   name="selected_category_id[]"
                                                   class="form-control"
                                                   value="{{$orderDetail->category_id}}"/>
                                            <input type="hidden"
                                                   name="selected_service_id[{{$orderDetail->category_id}}][]"
                                                   class="form-control"
                                                   value="{{$orderDetail->service_id}}"/>
                                            <input type="hidden"
                                                   name="selected_menu_item_id[{{$orderDetail->category_id}}][{{$orderDetail->service_id}}][]"
                                                   class="form-control"
                                                   value="{{$orderDetail->menu_item_id}}"/>
                                            <input type="hidden"
                                                   name="selected_menu_item_price[{{$orderDetail->category_id}}][{{$orderDetail->service_id}}][{{$orderDetail->menu_item_id}}]"
                                                   class="form-control"
                                                   value="{{$orderDetail->amount}}"/>
                                            <input type="hidden"
                                                   name="selected_menu_item_type[{{$orderDetail->category_id}}][{{$orderDetail->service_id}}][{{$orderDetail->menu_item_id}}]"
                                                   class="form-control"
                                                   value="{{$orderDetail->type}}"/>
                                            <input type="hidden" name="selected_category_name" class="form-control"
                                                   value="{{ $order->order_type }}"/>


                                        </div>
                                        <div id="priceofAllMenuItems">
                                            @foreach($menuItems as $menu)
                                                <input type="hidden" value="{{$menu->price}}"
                                                       id="selectedMenuItemPrice{{$menu->id}}"/>
                                            @endforeach
                                        </div>
                                        <div id="typeOfAllMenuItems">
                                            @foreach($menuItems as $menu)
                                                <input type="hidden" value="{{$menu->service_type}}"
                                                       id="selectedMenuItemType{{$menu->id}}"/>
                                            @endforeach
                                        </div>
                                        <div class="col-md-6 disabledDiv">
                                            <label for="customer_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Client') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="customer_id"
                                                    class="form-control customers @error('customer_id') is-invalid @enderror"
                                                    name="customer_id" autocomplete="customer_id">
                                                @if(!empty($employees))
                                                    @foreach($employees as $employee)
                                                        @if($employee->id == $order->customer_id)
                                                            <option
                                                                value="{{$employee->id}}"
                                                                data-delivery-charges="{{$employee->area['price']}}" {{$employee->id == $order->customer_id ? 'selected': ''}}>{{ ucfirst($employee->fullName())}}</option>
                                                        @endif
                                                    @endforeach
                                                @endif
                                            </select>

                                            @error('customer_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6 disabledDiv">
                                            <label for="category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Categories') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="category_id"
                                                    class="form-control categories @error('category_id') is-invalid @enderror"
                                                    name="category_id[]" autocomplete="category_id">
                                                <option value="">Select a Category</option>
                                                @if(!empty($categories))
                                                    @foreach($categories as $category)
                                                        <option value="{{ $category->id }}"
                                                                id="selectedCategory{{$category->id}}"
                                                        @foreach($categoryIds as $categoryId)
                                                            {{$categoryId == $category->id ? 'selected' : ''}}
                                                            @endforeach
                                                        >{{ucfirst($category->name)}}</option>
                                                    @endforeach
                                                @endif

                                            </select>
                                            @error('category_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 disabledDiv">
                                            <label for="service_category_id"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Service') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="service_category_id"
                                                    class="form-control service_category @error('service_category_id') is-invalid @enderror js-example-basic-multiple"
                                                    name="service_category_id[]" autocomplete="service_category_id">
                                                @foreach($services as $service)
                                                    <option value="{{$service->id}}"
                                                            id="selectedServices{{$service->id}}"
                                                    @foreach($serviceIds as $serviceId)
                                                        {{$serviceId == $service->id ? 'selected' : ''}}
                                                        @endforeach
                                                    >{{$service->name}}</option>
                                                @endforeach
                                            </select>

                                            @error('service_category_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <div id="log"></div>
                                        </div>
                                        <div class="col-md-6 disabledDiv">
                                            <label for="menu_items_id"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Menu Items') }}
                                                <span class="mandatorySign">*</span></label>

                                            <select id="menu_items_id"
                                                    class="form-control menu-items @error('menu_items_id') is-invalid @enderror"
                                                    name="menu_items_id[]" autocomplete="menu_items_id">
                                                @foreach($menuItems as $menu)
                                                    <option value="{{$menu->id}}"
                                                            id="selectedMenuItem{{$menu->id}}"
                                                    @foreach($menuItemsIds as $menuItemsId)
                                                        {{$menuItemsId == $menu->id ? 'selected' : ''}}
                                                        @endforeach
                                                    >{{$menu->name}}</option>
                                                @endforeach
                                            </select>

                                            @error('menu_items_id')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                            <div id="log"></div>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <div class="col-md-4 disabledDiv">
                                            <label for="net_price"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Net Price') }}
                                                <span> / {{ lcfirst($orderDetail->type) }}</span>
                                                <span class="mandatorySign">*</span></label>
                                            <input id="net_price" type="number"
                                                   class="form-control @error('net_price') is-invalid @enderror"
                                                   name="net_price"
                                                   value="{{(int)$order->order_details->sum('amount')}}"
                                                   autocomplete="net_price" autofocus>

                                            @error('net_price')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4">
                                            <label for="datetimepicker1" class="col-md-4 col-form-label text-md-left">Date
                                                Time <span class="mandatorySign">*</span></label>

                                            <input type="text" name="datetimepicker1" id="datetimepicker1"
                                                   class="form-control @error('datetimepicker1') is-invalid @enderror"
                                                   value="{{\Carbon\Carbon::parse($orderDetail->date)->format('m-d-Y')}}"
                                                   required autocomplete="datetimepicker1">
                                            @error('datetimepicker1')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>
                                        <div class="col-md-4">
                                            <label for="time" class="col-md-4 col-form-label text-md-left">Time <span
                                                    class="mandatorySign">*</span></label>

                                            <input type="text" name="time" id="time"
                                                   class="form-control @error('time') is-invalid @enderror"
                                                   value=" {{\Carbon\Carbon::parse($orderDetail->time)->format('h:i A')}}"
                                                   required autocomplete="time">


                                            <span class="invalid-feedback" role="alert" style="display: none">
                                                    <strong>Please add time with gap of 2 hrs from now</strong>
                                                </span>
                                            @error('time')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror

                                        </div>

                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">
                                            <label for="time" class="col-md-4 col-form-label text-md-left">Payment Mode
                                                <span
                                                    class="mandatorySign">*</span></label>
                                            <input type="radio" name="payment_mode" style="display:{{ ($order->order_type == 'onsite') ? 'none' : '' }}"
                                                   class="form-control @error('payment_mode') is-invalid @enderror" readonly
                                                   id="payment_mode"
                                                   value="Cash" {{ ($order->payment_mode == 'Cash') ? 'checked' : '' }}>
                                            <span style="display:{{ ($order->order_type == 'onsite') ? 'none' : '' }}">Cash</span>
                                            <input type="radio" name="payment_mode" readonly
                                                   class="form-control @error('payment_mode') is-invalid @enderror"
                                                   id="payment_mode"
                                                   value="EasyPaisa" {{ ($order->payment_mode == 'EasyPaisa') ? 'checked' : '' }}>
                                            EasyPaisa
                                            <input type="radio" name="payment_mode" readonly
                                                   class="form-control @error('payment_mode') is-invalid @enderror"
                                                   id="payment_mode"
                                                   value="Bank" {{ ($order->payment_mode == 'Bank') ? 'checked' : '' }}>
                                            Bank

                                            @error('payment_mode')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-6" id="hideduration"
                                             style="display: {{ ($orderDetail->type == 'Hourly') ? '' :'none' }}">
                                            <label for="duration"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Duration') }}
                                                <span class="mandatorySign">*</span></label>
                                            <select id="duration"
                                                    class="form-control duration @error('duration') is-invalid @enderror"
                                                    name="duration" autocomplete="duration">

                                                <option value="1" {{ (($orderDetail->duration/60) == 1) ? 'selected' :''  }}>1 h</option>
                                                <option value="2" {{ (($orderDetail->duration/60) == 2) ? 'selected' :''  }}>2 h</option>
                                                <option value="3" {{ (($orderDetail->duration/60) == 3) ? 'selected' :''  }}>3 h</option>
                                                <option value="4" {{ (($orderDetail->duration/60) == 4) ? 'selected' :''  }}>4 h</option>
                                                <option value="5" {{ (($orderDetail->duration/60) == 5) ? 'selected' :''  }}>5 h</option>
                                                <option value="6" {{ (($orderDetail->duration/60) == 6) ? 'selected' :''  }}>6 h</option>
                                                <option value="7" {{ (($orderDetail->duration/60) == 7) ? 'selected' :''  }}>7 h</option>
                                                <option value="8" {{ (($orderDetail->duration/60) == 8) ? 'selected' :''  }}>8 h</option>
                                                <option value="9" {{ (($orderDetail->duration/60) == 9) ? 'selected' :''  }}>9 h</option>
                                                <option value="10" {{ (($orderDetail->duration/60) == 10) ? 'selected' :''  }}>10 h</option>


                                            </select>

                                            @error('duration')
                                            <span class="invalid-feedback" role="alert">
                                                 <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12">

                                            <label for="alternate_address" class="col-md-4 col-form-label text-md-left">Alternate
                                                Address for This Service</label>
                                            <textarea class="form-control" id="alternate_address"
                                                      name="alternate_address"
                                                      rows="3">{{ $orderDetail->alternate_address }}</textarea>

                                        </div>
                                    </div>

                                    <hr>
                                    <div class="form-group row">
                                        <div class="col-md-12">
                                            <p><strong>Customer Information</strong></p>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-4 disabledDiv">
                                            <label for="mobile_number"
                                                   class="col-md-6 col-form-label text-md-left">{{ __('Mobile Number') }}</label>
                                            <input id="mobile_number" type="tel"
                                                   class="form-control @error('mobile_number') is-invalid @enderror"
                                                   name="mobile_number" value="{{ $order->phone_number }}"
                                                   autocomplete="mobile_number" autofocus>

                                            @error('mobile_number')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4 disabledDiv">
                                            <label for="city_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('City') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="city_name" type="text"
                                                   class="form-control @error('city_id') is-invalid @enderror"
                                                   name="city_name" value="{{$order->city->name}}"
                                                   autocomplete="city_name" autofocus>
                                            <input id="city_id" type="text" hidden
                                                   class="form-control @error('city_id') is-invalid @enderror"
                                                   name="city_id" value="{{$order->city_id }}"
                                                   autocomplete="city_id" autofocus>

                                            @error('city_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                        <div class="col-md-4 disabledDiv">
                                            <label for="area_name"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Area') }} <span
                                                    class="mandatorySign">*</span></label>
                                            <input id="area_name" type="text"
                                                   class="form-control @error('area_name') is-invalid @enderror"
                                                   name="area_name" value="{{$order->area->name}}"
                                                   autocomplete="area_name" autofocus>
                                            <input id="area_id" type="text" hidden
                                                   class="form-control @error('area_id') is-invalid @enderror"
                                                   name="area_id" value="{{$order->area_id}}"
                                                   autocomplete="area_id" autofocus>

                                            @error('area_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="form-group row">

                                        <div class="col-md-12 disabledDiv">
                                            <label for="address"
                                                   class="col-md-4 col-form-label text-md-left">{{ __('Address') }}
                                                <span class="mandatorySign">*</span></label>
                                            <input id="address" type="text"
                                                   class="form-control @error('address') is-invalid @enderror"
                                                   name="address" value="{{ $order->address }}"
                                                   autocomplete="address" autofocus>

                                            @error('address')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                            @enderror
                                        </div>


                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6">

                                            <label for="order_status" class="col-md-4 col-form-label text-md-left">Order
                                                Status</label>
                                            <select id="order_status"
                                                    class="form-control order-status-change @error('order_status') is-invalid @enderror"
                                                    name="order_status" autocomplete="order_status"
                                                    data-order-id="{{$order->id}}">
                                                <option value="">Select an option</option>
                                                <option
                                                    value="pending" {{ ($order->order_status == 'pending') ? 'selected' : '' }}>
                                                    Pending
                                                </option>
                                                <option
                                                    value="confirmed" {{ ($order->order_status == 'confirmed') ? 'selected' : '' }}>
                                                    Confirmed
                                                </option>
                                                <option
                                                    value="completed" {{ ($order->order_status == 'completed') ? 'selected' : '' }}>
                                                    Completed
                                                </option>
                                            </select>

                                        </div>
                                        <div class="col-md-6">

                                            <label for="staff_id" class="col-md-4 col-form-label text-md-left">Staff
                                                Assigned
                                            </label>
                                            <select id="staff_id"
                                                    class="form-control assign-staff @error('staff_id') is-invalid @enderror"
                                                    name="staff_id" autocomplete="staff_id"
                                                    data-order-id="{{$order->id}}" {{ ($order->order_status == 'pending' && ( $order->payment_mode == 'Bank' || $order->payment_mode == 'EasyPaisa' )) ? 'disabled' : '' }}>
                                                <option value="">Select an option</option>
                                                @if(!empty($staffs))
                                                    @foreach($staffs as $staff)
                                                        <option
                                                            value="{{$staff->id}}" {{ ($order->staff_id == $staff->id) ? 'selected' : ''}}>{{ucfirst($staff->fullName())}}
                                                            @if(!empty($todaysOrders))
                                                                @foreach($todaysOrders as $todayOrder)
                                                                    @if($staff->id == $todayOrder->staff_id)
                                                                        ({{ $todayOrder->currentOrders }} orders)
                                                                    @endif
                                                                @endforeach
                                                            @endif

                                                        </option>
                                                    @endforeach
                                                @endif
                                            </select>

                                        </div>


                                    </div>
                                </div>
                            </div>
                            <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                                <div class="m-form__actions m-form__actions">
                                    <a href="{{ route('admin.get.order.history') }}" class="btn btn-info">Back</a>
                                    <input type="button" id="get-orders" class="btn btn-primary"
                                           onclick="getOrder()" value="{{ __('SAVE') }}">
                                </div>
                            </div>
                        </form>
                        <!--end::Form-->
                    </div>
                    <!--end::Portlet-->
                </div>
            </div>
        </div>
    </div>
@endsection

@push('models')
    <!-- Modal -->
    <div class="modal fade orderModal" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
         aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Are you sure you want to update this
                        order?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row subtotal_section">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                            <h5><strong>Sub Total </strong></h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                            <h5><strong>Rs. <span
                                        id="subtotal_price">0</span></strong>
                            </h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="row">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                            <h5><strong>Delivery Charges: </strong></h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                            <h5><strong>Rs. <span id="delivery_charges">0</span></strong>
                            </h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="row grand_total">
                        <div class="col-md-6 col-sm-6 col-xs-6 text-left">
                            <h5><strong>Grand Total: </strong></h5>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-6 text-right">
                            <h5><strong>Rs. <span
                                        id="grand_total">0</span></strong>
                            </h5>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn buttonMain hvr-bounce-to-right" data-dismiss="modal">Close</button>
                    <a href="#" class="btn buttonMain hvr-bounce-to-right" id="order-submit">Yes</a>
                </div>
            </div>
        </div>
    </div>
@endpush

@push('js')
    <script>

        $(document).ready(function () {
            $('#time_zone').val(Intl.DateTimeFormat().resolvedOptions().timeZone);

            var dateToday = new Date();
            $('#datetimepicker1').datetimepicker({
                format: "MM-DD-YYYY",
                // minDate: moment(),
                icons: {
                    time: 'fa fa-clock',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-check',
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            });

            $('#time').datetimepicker({
                format: "hh:mm A",
                // minDate: moment().add(2, 'hours'),
                icons: {
                    time: 'fa fa-clock',
                    date: 'fa fa-calendar',
                    up: 'fa fa-chevron-up',
                    down: 'fa fa-chevron-down',
                    previous: 'fa fa-chevron-left',
                    next: 'fa fa-chevron-right',
                    today: 'fa fa-check',
                    clear: 'fa fa-trash',
                    close: 'fa fa-times'
                }
            });
        });
        var delivery_charges = 0;
        $('.customers').select2({});
        $('.categories').select2();
        $('.service_category').select2();
    </script>
    <script>
        $('.customers').change(function () {
            form = $(this).closest('form');
            node = $(this);

            var customer_id = $(this).val();
            var request = "customer_id=" + customer_id;
            $('#net_price').val(0);
            $('#grand_total').val(0);

            if (customer_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.customerServiceCategory') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            $('#mobile_number').val(response.data.user.phone_number);
                            $('#address').val(response.data.user.address);
                            $('#city_id').val(response.data.user.city_id);
                            $('#area_id').val(response.data.user.area_id);
                            $('#city_name').val(response.data.city_name);
                            $('#area_name').val(response.data.area_name);
                            $('#alternate_address').val(response.data.user.address);
                            $('.delivery_charges').val(response.data.area_price);

                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                toastr['error']("Please select Client");
            }
        });
        $('.categories').change(function () {

            $('#finalService').empty();
            $('.service_category').empty();
            $('.menu-items').empty();
            $("#priceofAllMenuItems").empty();
            $("#typeOfAllMenuItems").empty();
            $('#net_price').val(0);
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.service_category';
            var category_id = $(this).val();
            var request = "category_id=" + category_id;
            if (category_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.serviceCategory') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";

                            $.each(response.data.service_category, function (i, obj) {
                                html += '<option value="' + obj.id + '" id="selectedServices' + obj.id + '">' + obj.name + '</option>';
                            });
                            $(node_to_modify).html(html);
                            $(node_to_modify).prepend("<option value='' selected>Select Services</option>");
                            $('.js-example-basic-multiple').select2();
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value=''>Select Services</option>");
            }
        });
        $('.service_category').change(function () {
            $('#finalService').empty();
            $('.menu-items').empty();
            $('#net_price').val(0);
            form = $(this).closest('form');
            node = $(this);
            node_to_modify = '.menu-items';
            var service_id = $(this).val();
            var request = "service_id=" + service_id;
            $('#net_price').val(0);
            $('#grand_total').val(0);

            if (service_id !== '') {
                $.ajax({
                    type: "GET",
                    url: "{{ route('ajax.serviceSubCategory') }}",
                    data: request,
                    dataType: "json",
                    cache: true,
                    success: function (response) {
                        if (response.status == "success") {
                            var html = "";
                            var priceInputHtml = "";
                            var typeInputHtml = "";

                            html += '<option value="" selected>Select Menu Items</option>';
                            $.each(response.data.sub_category, function (i, obj) {
                                html += '<option value="' + obj.id + '" id="selectedMenuItem' + obj.id + '">' + obj.name + '</option>';
                                priceInputHtml += '<input type="hidden" value="' + obj.price + '" id="selectedMenuItemPrice' + obj.id + '"/>'
                                typeInputHtml += '<input type="hidden" value="' + obj.service_type + '" id="selectedMenuItemType' + obj.id + '"/>'
                            });
                            $(node_to_modify).html(html);
                            $("#priceofAllMenuItems").html(priceInputHtml);
                            $("#typeOfAllMenuItems").html(typeInputHtml);
                        }
                    },
                    error: function () {
                        toastr['error']("Something Went Wrong.");
                    }
                });
            } else {
                $(node_to_modify).html("<option value=''disabled>Select Menu Items</option>");
            }
        });
        $('.menu-items').change(function () {

            var category_id = $('#category_id').val();
            var category_text = $('#category_id option:selected').text();

            var service_id = $('#service_category_id').val();
            var service_text = $('#service_category_id option:selected').text();
            var menu_item_id = $('#menu_items_id').val();
            var menu_item_text = $('#selectedMenuItem' + menu_item_id + '').text();
            var menu_item_price = $('#selectedMenuItemPrice' + menu_item_id + '').val();
            var menu_item_type = $('#selectedMenuItemType' + menu_item_id + '').val();


            if (menu_item_id !== '') {
                $("#get-orders").removeAttr("disabled");
                var net_price = $('#net_price').val();
                $('#net_price').val(parseInt(net_price) + parseInt(menu_item_price));
                // $('#total_packages_price').text($('#net_price').val());
                dynamic_field(category_id, category_text, service_id, service_text, menu_item_id, menu_item_text, menu_item_price, menu_item_type);
            } else {
                $("#get-orders").attr("disabled", true);
                var net_price = $('#net_price').val();
                $('#net_price').val(0);
                // $('#total_packages_price').text($('#net_price').val());
                // $('#boxwithCategoryService' + category_id + '' + service_id + '' + oldUnselectValue + '').remove();
            }
        });
        $('.duration').change(function () {
            var menu_item_id = $('#menu_items_id').val();
            var menu_item_price = $('#selectedMenuItemPrice' + menu_item_id + '').val();
            var duration = $(this).val();
            $('#net_price').val(parseInt(duration) * parseInt(menu_item_price));
        });

        function getOrder() {
            var netPrice = parseInt($('#net_price').val());
            var category_text = $('#category_id option:selected').text();

            if (category_text == 'Online') {
                var delivery_charges = 0;
            } else {
                var delivery_charges = parseInt($('.delivery_charges').val());
            }

            $('#delivery_charges').text(delivery_charges);
            $('.delivery_charges').val(delivery_charges);
            $('#subtotal_price').text(netPrice);
            netPrice = netPrice + delivery_charges;
            $('#grand_total').text(netPrice);
            $('#grand_total_price').val(netPrice);
            $('#exampleModal').modal('show');
        }

        $('#order-submit').click(function (e) {


            e.preventDefault();
            let form = $('#add-orders');
            var request = $(form).serialize();

            $.ajax({
                type: "POST",
                url: "{{ route('admin.order.update') }}",
                data: request,
                dataType: "json",
                cache: false,
                success: function (response) {
                    toastr[response.data.flash_status](response.data.flash_message);

                    if (response.data.flash_status == "success") {
                        window.location = '{{route('admin.get.order.history')}}';
                    }

                },
                error: function () {
                    toastr['error']("Something Went Wrong.");
                }
            });

        });
    </script>
    <script>
        $('.order-status-change').change(function () {
            form = $(this).closest('form');
            node = $(this);
            var order_status = $(this).val();
            var payment_status = $('#payment_status').val();
            var order_id = $(this).data('order-id');
            var request = {"order_status": order_status, "order_id": order_id};
            if (order_status == 'confirmed' && payment_status == 'paid') {

                $('#get-orders').prop('disabled', true);
                $('#staff_id').prop('disabled', false);
                $('#staff_id').prop('required', true);
                $('#driver_id').prop('required', true);
                toastr['info']("Please Select Staff  for Order Confirmation.");
            } else {
                $('#get-orders').prop('disabled', false);
                $('#staff_id').prop('disabled', true);
                // toastr['error']("Please Select Order Status");
            }
        });
        $('.assign-staff').change(function () {
            var staff = $(this).val();
            var driver = $('.assign-driver').val();

            if (staff != '' && driver != '') {
                $('#get-orders').prop('disabled', false);
            } else {
                $('#get-orders').prop('disabled', true);
            }


        });
    </script>

    <script>
        var count = 1;

        function f(category_id, category_text, service_id, service_text, menu_item_id, menu_item_text, menu_item_price, menu_item_type) {
            // html = '<tr id="boxwithCategoryService' + category_id + '' + service_id + '">';
            // html += '<td style="display: none"><input type="hidden" name="selected_category_id[]" class="form-control" value="' + category_id + '" /></td>';
            // html += '<td style="display: none"><input type="hidden" name="selected_service_id[' + category_id + '][]" class="form-control" value="' + service_id + '" /></td>';
            // html += '<td style="display: none"><input type="hidden" name="selected_service_price[' + category_id + '][' + service_id + ']" class="form-control" value="' + service_price + '" /></td>';
            // html += '<td><input type="text"   class="form-control"   value="' + category_text + '" readonly/></td>';
            // html += '<td><input type="text"   class="form-control"   value="' + service_text + '" readonly/></td>';
            // html += '<td><input type="text"   class="form-control"   value="' + service_price + '"readonly/></td>';
            // html += '<td><button type="button" name="remove" id="" class="btn btn-danger remove" onclick="removeSelectedService(' + category_id + ',' + service_id + ',' + service_price + ')">Remove</button></td></tr>';
            // $('tbody').append(html);
        }

        function dynamic_field(category_id, category_text, service_id, service_text, menu_item_id, menu_item_text, menu_item_price, menu_item_type) {

            var type = ''
            if (menu_item_type == 'hourly') {

                type = 'hour';
                $('#hideduration').show();
            } else {
                type = 'fixed';
                $('#hideduration').hide();
            }
            if (category_text == 'Online') {
                category_text = 'online';
            } else {
                category_text = 'onsite';
            }

            $('#type-of-service').text('/' + type);
            var html = '';
            html += '<input type="hidden" name="selected_category_id[]" class="form-control" value="' + category_id + '"/>';
            html += '<input type="hidden" name="selected_category_name" class="form-control" value="' + category_text + '"/>';
            html += '<input type="hidden" name="selected_service_id[' + category_id + '][]" class="form-control" value="' + service_id + '"/>';
            html += '<input type="hidden" name="selected_menu_item_id[' + category_id + '][' + service_id + '][]" class="form-control" value="' + menu_item_id + '"/>';
            html += '<input type="hidden" name="selected_menu_item_price[' + category_id + '][' + service_id + '][' + menu_item_id + ']" class="form-control" value="' + menu_item_price + '"/>';
            html += '<input type="hidden" name="selected_menu_item_type[' + category_id + '][' + service_id + '][' + menu_item_id + ']" class="form-control" value="' + menu_item_type + '"/>';


            $('#finalService').html(html);


            // var current_address = $('#address').val();
            // var html = "";
            // html += ' <div class="check-duplicates" data-duplicate="' + category_id + '' + service_id + '' + menu_item_id + '" id="boxwithCategoryService' + category_id + '' + service_id + '' + menu_item_id + '">';
            // html += '<input type="hidden" name="selected_category_id[]" class="form-control" value="' + category_id + '"/>';
            // html += '<input type="hidden" name="selected_service_id[' + category_id + '][]" class="form-control" value="' + service_id + '"/>';
            // html += '<input type="hidden" name="selected_menu_item_id[' + category_id + '][' + service_id + '][]" class="form-control" value="' + menu_item_id + '"/>';
            // html += '<input type="hidden" name="selected_menu_item_price[' + category_id + '][' + service_id + '][' + menu_item_id + ']" class="form-control" value="' + menu_item_price + '"/>';
            // html += '<input type="hidden" name="selected_menu_item_type[' + category_id + '][' + service_id + '][' + menu_item_id + ']" class="form-control" value="' + menu_item_type + '"/>';
            //
            // html += '<div class="form-group row">';
            // html += '<div class="col-md-12">';
            // html += '<button type="button" name="remove"  id="" class="btn btn-danger remove removeBtn" onclick="removeSelectedService(' + category_id + ',' + service_id + ',' + menu_item_id + ',' + menu_item_price + ')">Remove </button>';
            // html += '</div>';
            // html += '</div>';
            //
            //
            // html += '<div class="form-group row">';
            //
            // html += '<div class="col-md-4 disabledDiv">';
            // html += '<label  class="col-md-4 col-form-label text-md-left">Category Name</label>';
            // html += '<input type="text" class="form-control" value="' + category_text + '">';
            // html += '</div>';
            //
            // html += '<div class="col-md-4 disabledDiv">';
            // html += '<label  class="col-md-4 col-form-label text-md-left">Service Name</label>';
            // html += '<input type="text" class="form-control" value="' + service_text + '" readonly/>';
            // html += '</div>';
            //
            // html += '<div class="col-md-4 disabledDiv">';
            // html += '<label  class="col-md-6 col-form-label text-md-left">Menu Item Name</label>';
            // html += '<input type="text" class="form-control" value="' + menu_item_text + '" readonly/>';
            // html += '</div>';
            //
            //
            // html += '</div>';
            //
            // html += '<div class="form-group row">';
            //
            //
            // html += '<div class="col-md-4 disabledDiv">';
            // html += '<label  class="col-md-6 col-form-label text-md-left">Price / ' + type + '</label>';
            // html += '<input type="text" class="form-control" value="' + menu_item_price + '" readonly/>';
            // html += '</div>';
            //
            // html += '<div class="col-md-4">';
            // html += '<label for="datetimepicker1" class="col-md-4 col-form-label text-md-left">Date<span class="mandatorySign">*</span></label>';
            // html += '<input type="text" name="selected_service_datetimepicker1[' + category_id + '][' + service_id + '][' + menu_item_id + ']" id="selected_service_datetimepicker1' + category_id + '' + service_id + '' + menu_item_id + '" class="form-control " required autocomplete="datetimepicker1" value="">';
            // html += '</div>';
            //
            // html += '<div class="col-md-4">';
            // html += '<label for="time" class="col-md-4 col-form-label text-md-left">Time<span class="mandatorySign">*</span></label>';
            // html += '<input type="text" name="selected_service_time[' + category_id + '][' + service_id + '][' + menu_item_id + ']" id="selected_service_time' + category_id + '' + service_id + '' + menu_item_id + '" class="form-control " required autocomplete="time" value="">';
            // html += '</div>';
            //
            // html += '</div>';
            //
            //
            // html += '<div class="form-group row">';
            //
            // html += '<div class="col-md-12">';
            // html += '<label for="alternate_address" class="col-md-4 col-form-label text-md-left">Alternate Address </label>';
            // html += '<textarea class="form-control" id="alternate_address"  name="selected_service_alternate_address[' + category_id + '][' + service_id + '][' + menu_item_id + ']" rows="3">' + current_address + '</textarea>';
            // html += '</div>';
            //
            // html += '</div>';
            //
            // html += '<hr>';
            // html += '</div>'
            // $('.service-append').append(html);
            // dateTimePicker('#selected_service_datetimepicker1' + category_id + '' + service_id + '' + menu_item_id + '', '#selected_service_time' + category_id + '' + service_id + '' + menu_item_id + '');


        }
    </script>
@endpush


