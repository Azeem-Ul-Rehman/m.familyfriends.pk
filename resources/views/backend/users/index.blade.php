@extends('layouts.master')
@section('title','Users')
@section('content')

    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Users
                        </h3>
                    </div>
                </div>

                <div class="m-portlet__head-tools">
                    <ul class="m-portlet__nav">
                        <li class="m-portlet__nav-item">
                            <a href="{{ route('admin.users.create') }}"
                               class="btn btn-accent m-btn m-btn--custom m-btn--pill m-btn--icon m-btn--air">
                                <span>
                                    <i class="la la-plus"></i>
                                    <span>Add User</span>
                                </span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

            <div class="m-portlet__body">
                <input type="hidden">

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>

                        <th> Sr NO.</th>
                        <th> Full Name</th>
                        <th> Email</th>
                        <th> Role</th>
                        <th> Total Spendings</th>
                        <th> Status</th>
                        <th> Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($users))
                        @foreach($users as $user)
                            @if($user->user_type != 'admin')
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{ucfirst($user->fullname())}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{ucfirst($user->user_type)}}</td>
                                    <td>{{$user->orders->sum('total_price')}}</td>

                                    <td style="width: 10%;">{{ucfirst($user->status)}}</td>
                                    <td nowrap style="width: 12%;">
                                        <a href="{{route('admin.users.edit',$user->id)}}"
                                           class="btn btn-sm btn-info pull-left ">Edit</a>
                                    </td>
                                </tr>
                            @endif
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection
@push('js')
    <script>
        $("#m_table_1").dataTable({
            // "ordering": false,
            "order": [[0, "asc"]],
            "columnDefs": [
                {orderable: false, targets: [6]}
            ],
        });

    </script>
@endpush
