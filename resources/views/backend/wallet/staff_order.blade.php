@extends('layouts.master')
@section('title','Wallet')
@push('css')
    <style>
        .map-image {
            height: 60px;
            width: 60px;
            border-radius: 50px !important;
            margin-right: 15px;
            float: left;
        }

        .mw-100 {
            margin: 0 auto;
        }

    </style>
@endpush
@section('content')
    <div class="m-content">
        <!--Begin::Section-->
        <div class="m-portlet m-portlet--mobile">
            <div class="m-portlet__head">
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            Cash In Hand
                        </h3>
                    </div>
                </div>
                <br>
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <span>Provider : {{($get_all_payment_received_by_provider - $get_all_provider_given_amount) < 0 ? 0 : ($get_all_payment_received_by_provider - $get_all_provider_given_amount)}}</span>
                        </h3>
                    </div>
                </div>
                <div class="m-portlet__head-caption">
                    <div class="m-portlet__head-title">
                        <h3 class="m-portlet__head-text">
                            <span>Company : {{(($get_all_payment_received_by_company - $get_all_company_given_amount) - $get_company_share) < 0 ? 0 :(($get_all_payment_received_by_company - $get_all_company_given_amount) - $get_company_share)  }}</span>
                        </h3>
                    </div>
                </div>
            </div>
            <div class="m-portlet__body">
                {{--tableScroll--}}

                <table class="table table-striped- table-bordered table-hover table-checkable" id="m_table_1">
                    <thead>
                    <tr>
                        <th> Order No.</th>
                        <th> Name</th>
                        <th> Grand Total</th>
                        <th> Comission %</th>
                        <th>  Share</th>
                        <th> Received Amount</th>
                        <th> Provider Received Amount</th>
                        <th> Cr/Dr Amount</th>
                        <th>Payment</th>
                    </tr>
                    </thead>
                    <tbody>
                    @if(!empty($Orders))
                        @foreach($Orders as $key=>$order)
                            <tr>
                                <td><a href="{{route('admin.get.user.order.show',$order->id)}}">{{$order->order_id}}</a>
                                </td>
                                <td>{{$order->staff->fullName()}}</td>
                                <td>{{$order->total_price}}</td>
                                <td>{{$order->staff->comission->staff_comission}}</td>
                                <td>{{(int)(($order->total_price*$order->staff->comission->staff_comission)/100)  }}</td>
                                <td>{{(int)$order->received_amount}}</td>
                                <td>{{ (int)($order->provider_received_amount)   }}</td>
                                <td>{{(int)($order->staff_commission - ($order->provider_received_amount - $order->provider_give_amount))   }} </td>
                                <td>
                                    @if(($order->staff_commission - ($order->provider_received_amount - $order->provider_give_amount)) == 0)
                                    @else
                                    <button type="button" class="btn btn-primary btn-sm"
                                            onclick="getPayment('{{$order->id}}')">Payment
                                    </button> 
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>

            </div>
        </div>
    </div>
@endsection
@push('models')

    <div id="payment" class="modal fade mw-100 w-75" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Payment</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">
                    <form class="m-form" method="post" action="{{ route('admin.storepayment') }}" id="create"
                          enctype="multipart/form-data" role="form">
                        @csrf
                        <input type="hidden" id="txtorderid" name="order_id">
                        <div class="m-portlet__body">
                            <div class="m-form__section m-form__section--first">

                                {{--                                    <div class="form-group row">--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="received_amount"--}}
                                {{--                                                   class="col-md-12 col-form-label text-md-left">{{ __('Received Amount') }}</label>--}}
                                {{--                                            <input id="received_amount" type="number"--}}
                                {{--                                                   class="form-control @error('received_amount') is-invalid @enderror"--}}
                                {{--                                                   name="received_amount" value="0"--}}
                                {{--                                                   autocomplete="received_amount" autofocus min="0">--}}

                                {{--                                            @error('received_amount')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                    <strong>{{ $message }}</strong>--}}
                                {{--                                                </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="give_amount"
                                               class="col-md-12 col-form-label text-md-left">{{ __('Company Give Amount to Provider') }}</label>
                                        <input id="give_amount" type="number"
                                               class="form-control @error('give_amount') is-invalid @enderror"
                                               name="give_amount" value="{{(($get_all_payment_received_by_company - $get_all_company_given_amount) - $get_company_share) < 0 ? 0 :(($get_all_payment_received_by_company - $get_all_company_given_amount) - $get_company_share)  }}"
                                               autocomplete="give_amount" autofocus min="0" max="{{(($get_all_payment_received_by_company - $get_all_company_given_amount) - $get_company_share) < 0 ? 0 :(($get_all_payment_received_by_company - $get_all_company_given_amount) - $get_company_share)  }}">

                                        @error('give_amount')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>

                                {{--                                    <div class="form-group row">--}}

                                {{--                                        <div class="col-md-12">--}}
                                {{--                                            <label for="provider_received_amount"--}}
                                {{--                                                   class="col-md-12 col-form-label text-md-left">{{ __('Provider Received Amount') }}</label>--}}
                                {{--                                            <input id="provider_received_amount" type="number"--}}
                                {{--                                                   class="form-control @error('provider_received_amount') is-invalid @enderror"--}}
                                {{--                                                   name="provider_received_amount" value="0"--}}
                                {{--                                                   autocomplete="provider_received_amount" autofocus min="0">--}}

                                {{--                                            @error('provider_received_amount')--}}
                                {{--                                            <span class="invalid-feedback" role="alert">--}}
                                {{--                                                    <strong>{{ $message }}</strong>--}}
                                {{--                                                </span>--}}
                                {{--                                            @enderror--}}
                                {{--                                        </div>--}}
                                {{--                                    </div>--}}

                                <div class="form-group row">

                                    <div class="col-md-12">
                                        <label for="provider_give_amount"
                                               class="col-md-12 col-form-label text-md-left">{{ __('Provider Give Amount to Company') }}</label>
                                        <input id="provider_give_amount" type="number"
                                               class="form-control @error('provider_give_amount') is-invalid @enderror"
                                               name="provider_give_amount" value="{{$get_all_payment_received_by_provider - $get_all_provider_given_amount}}"
                                               autocomplete="provider_give_amount" autofocus min="0" max="{{$get_all_payment_received_by_provider - $get_all_provider_given_amount}}">

                                        @error('provider_give_amount')
                                        <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                        @enderror
                                    </div>
                                </div>


                            </div>
                        </div>
                        <div class="m-portlet__foot m-portlet__foot--fit text-md-right">
                            <div class="m-form__actions m-form__actions">
                                <a href="#" class="btn btn-info" data-dismiss="modal">Back</a>
                                <button type="submit" class="btn btn-primary">
                                    {{ __('SAVE') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>

            </div>

        </div>
    </div>
@endpush
@push('js')

    <script>


        var table = $("#m_table_1").DataTable({
            "order": [[0, "desc"]],
            "columnDefs": [
                {orderable: false, targets: [7]}
            ],
        });

        function getPayment(order_id) {
            $("#txtorderid").val(order_id);
            $("#payment").modal('show');
        }

    </script>

@endpush
